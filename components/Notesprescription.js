
// https://stackoverflow.com/questions/56715637/using-ajax-on-react-native   
import React,{Component} from 'react'
import {View,Text,StyleSheet,TouchableOpacity, Alert} from 'react-native'
import { TextInput } from 'react-native-paper';
import Appointment_for_today from './Appointment_for_today'
import Symptoms from './Symptoms'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
 class Notesprescription extends React.Component{

            render(){
                    return(
                            <View style={styles.container}>
                                <View style={{flex:1,margin:10}}>
                              <View style={styles.text}>
                              
                                <Text style={{fontSize:18}}>Notes/Prescription</Text>
                            
                                <TextInput
                                style={{position:'absolute',bottom:0,backgroundColor:'white',width:'100%'}}
                                    />  
                              </View>
                              <View style={styles.notes}>
                              <TouchableOpacity onPress={() => this.props.navigation.navigate('Appointment_for_today')} style={{alignSelf:'center',width:'90%',padding:10,borderWidth:2,borderColor:'#90d6a9',backgroundColor:'#90d6a9',borderRadius:30}}>
                        <Text style={{color:'white',fontSize:20,textAlign:'center',}}>More Consultation Required</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => Alert.alert("ypu clicked on button")} style={{alignSelf:'center',width:'90%',padding:10,borderWidth:2,borderColor:'#90d6a9',backgroundColor:'#90d6a9',borderRadius:30}}>
                        <Text style={{color:'white',fontSize:20,textAlign:'center',}}>Blood Samples Required</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() =>this.props.navigation.navigate('Symptoms')} style={{alignSelf:'center',width:'90%',padding:10,borderWidth:2,borderColor:'#90d6a9',backgroundColor:'#90d6a9',borderRadius:30}}>
                        <Text style={{color:'white',fontSize:20,textAlign:'center',}}>Wants Meds Delivered</Text>
                        </TouchableOpacity>
                              </View>
                              </View>
                            </View>
                        );

            }
}

const AppNavigator = createStackNavigator({
    Notesprescription:{
        screen:Notesprescription,
        navigationOptions: {
          header: null // Will hide header for all screens of current stack 
            
          }
                },
                             
                Appointment_for_today:{
      screen:Appointment_for_today,
     navigationOptions: {
       header: null // Will hide header for all screens of current stack 
         
       },
    },
       Symptoms:{
        screen:Symptoms,
       navigationOptions: {
         header: null // Will hide header for all screens of current stack 
           
         }
  
          },  
                   
          });
export default createAppContainer(AppNavigator);  


const styles=StyleSheet.create({
    container:{
        flex:1,
   
        padding:10,
        backgroundColor:'white'
    },
    text:{
        flex:1,
        backgroundColor:'cyan',
        justifyContent:'center',
        backgroundColor:'white'
    },
    notes:{
        flex:1,
        justifyContent:'space-evenly'
    }
})