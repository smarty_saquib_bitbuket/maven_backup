import React,{Component} from 'react'
import {View,Text,StyleSheet,Image,TouchableOpacity} from 'react-native'
import Paynow from './Paynow'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

class Profile_with_details extends React.Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.subcontainer}>
                 
                        <View style={styles.image}>
                        <Image
                        style={{borderWidth:1,borderRadius:50,width:'50%',height:'50%'}}
                        source={require('../assets/fruite.jpeg')}
                        />
                        </View>
              
                    <View style={styles.text_with_button}>
                  
                        <Text style={{fontSize:20,}}>
                        Ericka Mars
                        </Text>
                        <Text style={{color:'#B2DDc1',fontSize:18,}}>
                            LocationExpert
                            </Text>
                            <TouchableOpacity style={{backgroundColor:'#B2DDC1',borderRadius:20,marginTop:5,padding:5,width:'80%'}}>
                        <Text style={{color:'white',fontSize:15,textAlign:'center'}}>Watch Video</Text>
                    </TouchableOpacity>

                    </View>
                  
                </View>
                <View style={{flex:4, margin:20,backgroundColor:'white'}}>
                    <View style={{flex:1,justifyContent:'space-around'}}>
                            <Text style={{color:'#B2DDc1'}}>Certification:</Text>
                            <Text style={{color:'silver'}}>  It is a long established fact that a reader
                                     will be distracted by the readable
                                     content of a page when looking it's
                                    layout
                                </Text>
                        </View>
                        <View style={{flex:1,justifyContent:'space-evenly'}}>
                        <Text  style={{color:'#B2DDc1'}}>Education:</Text>
                        <Text style={{color:'silver'}}>  It is a long established fact that a reader
                                     will be distracted by the readable
                                     content of a page when looking it's
                                    layout
                                </Text>
                        </View>
                        <View style={{flex:1,justifyContent:'space-evenly'}}>
                        <Text  style={{color:'#B2DDc1'}}>Year of Experience:</Text>
                        <Text style={{color:'silver',justifyContent:'flex-start'}}>  12 years of Experience</Text>
                        </View>
                        <View style={{flex:1,justifyContent:'space-evenly'}}>
                        <Text  style={{color:'#B2DDc1'}}>Sub-specialities:</Text>
                        <Text style={{color:'silver'}}>  It is a long established fact that a reader
                                     will be distracted by the readable
                                     content of a page when looking it's
                                    layout
                        </Text>
                        </View>
                        <View style={{flex:1.5,justifyContent:'space-around'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Paynow')} style={{alignSelf:'center',width:'80%',padding:10,borderWidth:2,borderColor:'#90d6a9',backgroundColor:'#90d6a9',borderRadius:30}}>
                        <Text style={{color:'white',fontSize:20,textAlign:'center',}}>Book Doctor</Text>
                        </TouchableOpacity>
                        </View>
                </View>
                
            </View>
        );
    }
}

const AppNavigator = createStackNavigator({
    
    Profile_with_details:{
        screen:Profile_with_details,
        navigationOptions: {
          header: null // Will hide header for all screens of current stack 
            
          }
                },
                             
     Paynow:{
        screen:Paynow,
        navigationOptions: {
        header: null // Will hide header for all screens of current stack 
         
       }
  
          },  
                   
          });
export default createAppContainer(AppNavigator);    
const styles=StyleSheet.create({
container:{
    backgroundColor:'white',
    flex:1
},
subcontainer:{
    flex:2,
    backgroundColor:'white',
    flexDirection:'row',
    justifyContent:'space-evenly' ,
    margin:20,
    marginBottom:0,
    
},
image:{

    justifyContent:'center',
    alignItems:"center",
    flex:1,

},
text_with_button:{
 
    justifyContent:'center',
    alignItems:'flex-start',
    flex:3
    
}

})