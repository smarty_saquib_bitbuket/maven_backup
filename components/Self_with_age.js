import React,{Component} from 'react'
import {View,Text,StyleSheet,Image, Slider,Dimensions} from 'react-native'
import { Ionicons,FontAwesome,MaterialCommunityIcons} from '@expo/vector-icons'; // refer 
import { TouchableOpacity } from 'react-native-gesture-handler';
import { createAppContainer } from 'react-navigation';
import * as Font from 'expo-font';
import { createStackNavigator } from 'react-navigation-stack';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import Self_with_status from './Self_with_status';

class Self_with_age extends React.Component{
    constructor(props) {
        super(props)
        Font.loadAsync({
          'open-sans-Regular': require('../fonts/SourceSerifPro-Regular.ttf'),
        });
        
      }
    state={
        slideValue: 18,
        
    }
    

render(){
    

    return(
                <View style={styles.container}>

                   <View style={styles.img_view}>
                       <Image

                       style={{position:'absolute',bottom:20,alignSelf:'center',    height:heightPercentageToDP(6),
                       width:widthPercentageToDP(14),
                       }}
                       source={require('../assets/logo(3).png')}
                       />
                   </View>
                   <View style={styles.txt_view}>
                       <Text style={{position:'absolute',bottom:0,alignSelf:'center',fontSize:24,color:'#777E8B', fontFamily:'open-sans-Regular' ,}}>
                           TELL US ABOUT YOU
                       </Text>
                   </View>
                   <View style={styles.age_family}>
                       

                     <View style={{backgroundColor:'#b2ddc1',borderWidth:1,borderColor:'#f7f7f7',width:155,height:60,alignSelf:'center',}}>

<View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>  
 <FontAwesome style={{}} name="birthday-cake" size={24} color={'white'}/>

<Text style={{color:'white',paddingLeft:14,fontSize:17}}>Family</Text></View>
<View style={styles.TriangleShapeCSS} />
</View>
                       

                       <View style={{backgroundColor:'white',borderWidth:1,borderColor:'silver',width:155,height:60,alignSelf:'center',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                        <MaterialCommunityIcons name="ring" size={32} color={'silver'}/>
                        <Text style={{color:'silver',marginLeft:10,fontSize:heightPercentageToDP(1.9)}}>Family</Text>
                        </View>
                   </View>
                  
                  
                  
                   <View style={styles.age_scroll}>
                   <View style={{height:70,width:70,borderRadius:100,
                    borderWidth:0.9,borderColor:'#c4c4c4',alignItems:"center",
                    flex:1,justifyContent:"center",}}>
                        <Text style={{alignSelf:'center',fontSize:20,color:'#000000', textShadowOffset: {width: 1,height: 1},
      textShadowRadius: 1,
      textShadowColor:'#c4c4c4'}}> 
                        {this.state.slideValue}</Text></View>
                  
                  
                  
                  
                  
                  
                  
                  
                  
<View style={{flex: 1.2, flexDirection: 'row',borderRadius:5,justifyContent:'center',alignItems:'center'}}>
<Slider

minimumValue={18}
maximumValue={100}
step={1}
style={{flex: 0.95, height: 50, padding: 10,borderRadius:50}}
onValueChange={(value)=> this.setState({ slideValue: value}) }

minimumTrackTintColor='#9EDDCF'	
maximumTrackTintColor	='#9EDDCF'	
/></View>

                   </View>
                   <View style={styles.light_text}>
                       <Text style={{fontSize:10}}>
                           -  SLIDE TO CHOOSE  -
                       </Text>
                   </View>
                   <View style={styles.button}>
                     <TouchableOpacity onPress={()=>this.props.navigation.navigate('Self_with_status')} style={{    padding: 5,
                            height: 100,
                            width: 100,  //The Width must be the same as the height
                            borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
                            borderColor:'silver',
                            borderWidth:1,
                            justifyContent:'center',
                            
                         
                            }}>
                           <Text style={{color:'silver',fontSize:20,textAlign:'center',}}>NEXT</Text>
                           </TouchableOpacity>
                   </View>
                </View>

        );
}

}

const AppNavigator = createStackNavigator({
      Self_with_age:{
          screen:Self_with_age,
          navigationOptions: {
            header: null // Will hide header for all screens of current stack 
              
            }
                  },
                               
         Self_with_status:{
        screen:Self_with_status,
        navigationOptions: {
         header: null // Will hide header for all screens of current stack 
           
         }

            },  
                     
            });
export default createAppContainer(AppNavigator);
                
const styles=StyleSheet.create({
container:{
    flex:1,
 
},
img_view:{
    flex:2.3,
    
},
txt_view:{
    flex:1,
    
},
age_family:{
    flex:4,
    flexDirection:'row',
    justifyContent:'space-evenly',  
    
   
},
age_scroll:{
    flex:3,
  
  margin:10,
    justifyContent:"center",
    alignItems:'center',
    

    
  
},
TriangleShapeCSS: {
 
    width: 0,
    height: 0,
    borderLeftWidth: 23.5,
    borderRightWidth: 23.5,
    borderBottomWidth: 20,
    borderStyle: 'solid',
    backgroundColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#b2ddc1',
    borderTopColor:'#b2ddc1',
    overflow:"hidden",
    position: 'absolute',
        zIndex:1,
        top:57,
        left:50,
justifyContent:'center',

    
    transform: [
      {rotate: '180deg'}
    ]
  },
light_text:{
    flex:1,
    justifyContent:'center',
    flexDirection:'row',

},
button:{
    flex:4,
    justifyContent:'center',
    alignItems:'center',
    
    
},


})