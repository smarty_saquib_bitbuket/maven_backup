import React, { Component } from "react";
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
import { Dimensions, StyleSheet,View,Text,Image,TouchableOpacity } from 'react-native';

const { width: screenWidth } = Dimensions.get('window')

export default class Appointment_for_today extends Component {
  constructor() {
    super();

    this.state = {
      entries: [
        {
          "ID": "001", 
          "Name": "001", 
          "thumbnail": "https://wallpapercave.com/wp/KaNO7Ya.jpg"
      },

      {
          "ID": "002", 
          "Name": "002", 
          "thumbnail": "https://wallpapercave.com/wp/ZxV8qRo.jpg"
      },

      {
          "ID": "003", 
          "Name": "003", 
          "thumbnail": "https://wallpapercave.com/wp/ytM5xOy.jpg"
      }
      ,

      {
          "ID": "004", 
          "Name": "004", 
          "thumbnail": "https://wallpapercave.com/wp/STgHPst.jpg"
      }
      ,

      {
          "ID": "005", 
          "Name": "005", 
          "thumbnail": "https://wallpapercave.com/wp/vg5q7pY.jpg"
      }
      ,

      {
          "ID": "006", 
          "Name": "006", 
          "thumbnail": "https://wallpapercave.com/wp/b2HsGgL.jpg"
      }
      ],
  }
}
    _renderItem ({item, index}, parallaxProps) {
        console.log(item);    
        return (
        <View style={{height:200,backgroundColor:'red'}}>
                <View style={styles.card}>
                
                        <View style={{flexDirection:'row',margin:15,justifyContent:'center',alignItems:'center'}}>
                        <View style={{flexDirection:'column',flex:1,justifyContent:'center',alignItems:'center'}}>
                        <Text style={{fontSize:15}}>At 3:30 pm</Text>
                       <TouchableOpacity
                       style={{backgroundColor:'#B2DDC1',width:'80%',borderWidth:1,borderColor:'silver',padding:2,borderRadius:20,marginTop:10}}
                       >
                         <Text style={{fontSize:15,textAlign:'center'}}>in 2 Hours</Text>
                       </TouchableOpacity>
                        </View>
                        <View
                        style={{width:1,height:80,backgroundColor:'#B2DDC1',marginTop:10,}}
                        />
                        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                          
                        <Image
                        style={{width:60,height:60,borderRadius:50,}}
                        source={{uri:item.thumbnail}}
                        />
                        <Text style={{fontSize:15,}}>Ritika Amru</Text>
            
                        </View>
                        </View>
                   
                       

                        </View>
                        </View>

            
        );
    }

    render () {
        return (
          <View style={styles.container}>
            <View style={{height:200,backgroundColor:'orange'}}>
            <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}><Text style={{}}>Appointment_for_today</Text></View>
            <Carousel
                sliderWidth={screenWidth}
                sliderHeight={screenWidth}
                itemWidth={screenWidth - 60}
                data={this.state.entries}
                renderItem={this._renderItem}
                hasParallaxImages={true}
                
            />
          
          </View>
          </View>
          

        );
    }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    

    backgroundColor:'red',
  },  
card:{
  height:'60%',
  alignSelf:'center',
  alignSelf:'center',
  backgroundColor:'yellow',
  alignItems:'center',
   marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
   ...StyleSheet.absoluteFillObject,

    borderWidth:1,
    borderColor:'white',
    borderRadius:20,
  
    backgroundColor:'white',
   
}
})


// https://github.com/oblador/react-native-parallax
// https://stackoverflow.com/questions/57593529/react-native-dynamic-image-slider-transition-opacity