import React,{ Component } from 'react'
import {View,Text,Image, StyleSheet} from 'react-native'

class SplashScreen extends React.Component{

    render(){
        return(
                    <View style={styles.container}>
                        <Image 
                       style={styles.img} source={require("../logo1.png")}/>
                    </View>
        );
    }
}
export default SplashScreen
const styles=StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    img:{
        width:300,
        height:200
    }
})