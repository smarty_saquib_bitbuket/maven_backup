import React,{Component} from 'react'
import {View ,Text,StyleSheet,FlatList,Alert,Image,TouchableOpacity} from 'react-native'
import { Searchbar, IconButton } from 'react-native-paper';
import { Ionicons,FontAwesome,MaterialCommunityIcons,MaterialIcons,Entypo} from '@expo/vector-icons'; // refer 

import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import Icon from 'react-native-fontawesome';
import Profile_with_details from './Profile_with_details'
class Card_with_doctors extends React.Component{
  

   
    state={
                data:[
                    {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     },
                     {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     },
                     {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     },
                     {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     },
                     {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     },
                     {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     },
                     {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     },
                     {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     }


 
                    
                
                ]
    }
   

    render(){

        return(
            <View style={styles.container}>
            
            <FlatList  
            data={[  {title:'Rubin Shaikh',subtitle:'location expert',experience:'12 years of experience',avtar:require('../assets/fruite.jpeg'),
            },
            {title:'Rubin Shaikh',subtitle:'location expert',experience:'12 years of experience',avtar:require('../assets/fruite.jpeg'),
            },
            {title:'Rubin Shaikh',subtitle:'location expert',experience:'12 years of experience',avtar:require('../assets/fruite.jpeg'),
        },
        {title:'Rubin Shaikh',subtitle:'location expert',experience:'12 years of experience',avtar:require('../assets/fruite.jpeg'),
    },
    {title:'saquib Shaikh',subtitle:'location expert',experience:'12 years of experience',avtar:require('../assets/fruite.jpeg'),
}
          


            ]}  
            renderItem={({item}) =>  
                        <View style={styles.card}>
                        <View style={{flexDirection:'row',margin:15}}>
                        <View style={{flexDirection:'column',flex:2,}}>
                        <Text style={{fontSize:18}}>{item.title}</Text>
                        <Text style={{color:'#B2DDc1',}}>{item.subtitle}</Text>
                        <Text style={{color:'silver',fontSize:10}}>{item.experience}</Text>
                        </View>
                        
                        <View style={{flexDirection:'row',flex:1,justifyContent:'space-around',}}>
                            <View
                                style={{
                                borderLeftWidth: 1,
                                borderLeftColor: '#B2DCC1',
                                }}
                                />

                        <Image
                        style={{width:50,height:50}}
                        source={item.avtar}
                        />
                
                        </View>
                        </View>
                        <View style={{flex:1,}}>
                      <TouchableOpacity style={{alignSelf:'center',backgroundColor:'#B2DDC1',borderRadius:20,padding:5}}>
                        <Text style={{color:'white',fontSize:15,textAlign:'center'}}>Check Availability </Text>
                    </TouchableOpacity>
                        </View>
                       
                       </View>
                      }  
                    ItemSeparatorComponent={this.renderSeparator}  
           
        
            />  
            
               
             </View>

        );
    }

}



  class general extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text>general page</Text>
        </View>
      );
    }
  }



const TabNavigator = createBottomTabNavigator({
    Card_with_doctors: {
        
        screen:Card_with_doctors,
        navigationOptions:{
            tabBarLabel:'Find Care',
            tabBarIcon:({tintColor})=>(
                <FontAwesome name='heartbeat' color={tintColor} size={24}/>
            )
            
        }

    },
    Profile_with_details: {
        screen:Profile_with_details,
        navigationOptions:{
            tabBarLabel:'Profile',
            tabBarIcon:({tintColor})=>(
                <MaterialCommunityIcons name='file-document' color={tintColor} size={24}/>
            )
            
        }
    },
    general: {
        screen:general,
        navigationOptions:{
            tabBarLabel:'me',
            tabBarIcon:({tintColor})=>(
                <MaterialIcons name='person' color={tintColor} size={24}/>
            )
            
        }
    },
   
  },
  {
      initialRouteName:'Profile_with_details',
    
      tabBarOptions:{
          activeTintColor:'red',
          inactiveTintColor:'gray',
          style: {
            backgroundColor: 'white',//color you want to change
            borderTopWidth:0
          }

      }

    } );
  
export default createAppContainer(TabNavigator);
  
  

const styles=StyleSheet.create({
container:{
   flex:1,
backgroundColor:'white'
   
   
},
card:{
    
    flex:2,
    
    margin:10,
    borderRadius:5,
  
    flex:1,
    shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 4,
},
shadowOpacity: 0.32,
shadowRadius: 5.46,
backgroundColor:'white',
height:130,
elevation: 9,
borderRadius:20

}

})