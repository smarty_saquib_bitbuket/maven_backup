// import React,{Component} from 'react';
// import {View,Text,StyleSheet,TextInput,Button} from 'react-native';
// import { Ionicons,FontAwesome,MaterialCommunityIcons,MaterialIcons} from '@expo/vector-icons'; // refer 
// import Carousel, { Pagination } from 'react-native-snap-carousel';
// // var SinchClient = require('sinch-rtc');
// export default class Symtoms extends React.Component{


//     _renderItem ({item, index}) {
//         return <MySlideComponent data={item} />
//     }

//     get pagination () {
//         const { entries, activeSlide } = this.state;
//         return (
//             <Pagination
//               dotsLength={entries.length}
//               activeDotIndex={activeSlide}
//               containerStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.75)' }}
//               dotStyle={{
//                   width: 10,
//                   height: 10,
//                   borderRadius: 5,
//                   marginHorizontal: 8,
//                   backgroundColor: 'rgba(255, 255, 255, 0.92)'
//               }}
//               inactiveDotStyle={{
//                   // Define styles for inactive dots here
//               }}
//               inactiveDotOpacity={0.4}
//               inactiveDotScale={0.6}
//             />
//         );
//     }


//     calling=()=>{

//         alert("calling")

//     }
//             state={
//                 text:'',
//                 todos:[],
//                 status:false
//             }

//             AddTodos=()=>{
               
//               var newtodos=this.state.text;
//               var arr=this.state.todos
//               if(this.state.text==''){
//                  alert("field cannot be empty")
                 
//               }
            
              
//                 if(this.state.text!=''){
//                     if(arr.includes(newtodos)){
//                         alert("already exist");  
//                         this.setState({
//                             text:''
//                         }) 
//                     }
//                     else{
//                     arr.push(newtodos)
//                     this.setState({todos:arr})
//                     console.log("else execute")
//                     this.setState({
//                         text:''
//                     })
//                 }
//                 }
               
//                 this.textInput.clear()
              

             
//             }
            

//             DeleteTodos=(t)=>{
//                    var deletearr=this.state.todos
//                    var index = deletearr.indexOf(t);
//                    console.log(index);
//                     deletearr.splice(index,1)
//                    this.setState({
//                        todos:deletearr,
                      
                       

//                    })
//                    this.state.status=true
                   

//                 }

//             RenderTodos=()=>{
//                 console.log(this.state.todos)
//                 console.log(this.state.text)
              
//                 return this.state.todos.map(t=>{

//                     return(
                    
//                             <View style={{borderWidth:1,borderColor:'#B2DDc1',padding:5,borderRadius:20,backgroundColor:'#B2DDC1',flexDirection:'row'}}>
//                             <Text style={{fontSize:18,textAlign:'center',lineHeight:20}}>{t}{"  "}</Text><MaterialIcons name='cancel' size={20}  key={t} onPress={()=>this.DeleteTodos(t)}/>
//                             </View>
               
//                     )
//                 })
        
//         }
     

//             render(){

//                 return(
//                     <View style={styles.container}>
                     
//                        {/*
//                         <TextInput
//                         style={{borderWidth:1,borderColor:'red'}}
//                         placeholder='write somethimg!!!'
//                         onChangeText={(text)=>this.setState({text})}
//                         ref={input => { this.textInput = input }}
//                         />
//                         <Button
//                         title="Add Todos"
//                         color='green'
//                         onPress={this.AddTodos}
//                         />
//                         <View style={{flexDirection:'row',flexWrap:'wrap',justifyContent:'space-around'}}>
//                         {this.RenderTodos()}
//                        </View>*/}

//                       {/* <Button
//                       title='calling'
//                       onPress={this.calling}
//                       /> */}
//                 <Carousel
//                   data={this.state.entries}
//                   renderItem={this._renderItem}
//                   onSnapToItem={(index) => this.setState({ activeSlide: index }) }
//                 />
//                 { this.pagination }
            
//                     </View>
//                 );
//             }

// }


// const styles=StyleSheet.create({
//     container:{
//         justifyContent:'center',    
//         alignItems:'center',
       
//         flex:1,
        
//     }
// })

import React, { useState, useRef } from "react";
import { View,Text, Dimensions } from "react-native";
const { width: screenWidth, height: screenHeight } = Dimensions.get("window");
 import Carousel, { Pagination } from 'react-native-snap-carousel';

const myCarousel = () => {
const [activeSlide, setActiveSlide] = useState(0);
const carousel = useRef();
const entries = [
    {
        title: "Adidas"
    },
    {
        title: "Nike"
    },
    {
        title: "Puma"
    },
    {
        title: "Reebok"
    },
    {
        title: "Adidas"
    },
    {
        title: "Nike"
    },
    {
        title: "Puma"
    },
    {
        title: "Reebok"
    }
    
];

var slides = [];

const entriesSplitter = () => {
    let size = 2; //Based on the size you want
    while (entries.length > 0) {
        slides.push(entries.splice(0, size));
    }
};

// render every single slide
const _renderItem = ({ item,index }) => {
    return (
        <View style={{ flexDirection: "row", flexWrap: "wrap" , backgroundColor:'cyan'}}>
            {item.map(item => {
                return <Text key={index}>{item.title}</Text>;
            })}
        </View>
    );
};

return (
    <View>
        {entriesSplitter()}
        <Carousel
        activeOpacity={true}
        autoplay={true}
            ref={carousel}
            data={slides}
            renderItem={_renderItem}
            onSnapToItem={index => setActiveSlide(index)}
            sliderWidth={screenWidth}
            sliderHeight={screenHeight}
            itemWidth={screenWidth}
        />
        <Pagination
            dotsLength={4} // also based on number of sildes you want
            activeDotIndex={activeSlide}
            containerStyle={{ backgroundColor: "red", borderWidth: 2 }}
            dotStyle={{
                width: 10,
                height: 10,
                borderRadius: 5,
                marginHorizontal: 8,
                backgroundColor: "black"
            }}
            inactiveDotStyle={{
                backgroundColor: "pink"
            }}
            inactiveDotOpacity={0.4}
            inactiveDotScale={0.6}
        />
    </View>
);
};

export default myCarousel;