import React,{Component} from 'react'
import {View ,Text,StyleSheet,FlatList,Alert,Image} from 'react-native'
import { Searchbar, IconButton } from 'react-native-paper';
import { Ionicons,FontAwesome,MaterialCommunityIcons,MaterialIcons} from '@expo/vector-icons'; // refer 

import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import Icon from 'react-native-fontawesome';
class Tab_with_list extends React.Component{
    state={
                data:[
                    {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     },
                     {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     },
                     {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     },
                     {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     },
                     {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     },
                     {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     },
                     {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     },
                     {
                        avtar:require('../assets/fruite.jpeg'),
                        text:'General Health'

 
                     }


 
                    
                
                ]
    }
    renderSeparator = () => {  
        return (  
            <View  
                style={{  
                    height: 1,  
                    
                    marginLeft:30,
                    marginRight:30,
                    backgroundColor: "#000",  
                }}  
            />  
        );  
    };  
    //handling onPress action  
    getListViewItem = (item) => {  
        Alert.alert(item.key);  
    }  
    render(){

        return(
            <View style={styles.container}>
                <View style={{flexDirection:'column',justifyContent:'space-evenly',flex:2}}>
                <Text style={{fontSize:20,alignSelf:'center'}}>FIND CARE</Text>
                <Searchbar
                
               style={{padding:5,width:'100%',}}
                placeholder="Search by condition 
                or speciality"       
                />
                </View>
                <View style={{flex:6,marginTop:6,borderWidth:1,borderColor:'black',borderRadius:2}}>
                <FlatList  
                    data={this.state.data} 
                   
                                  renderItem={({item})=>
                    <View style={{flexDirection:'row',padding:30,justifyContent:'flex-start'}}>
                    <Image
                    style={{width:50,height:50,
                    }}
                    source={item.avtar}
                    />
                    <Text style={{lineHeight:50,fontSize:15,marginLeft:'10%'}}>{item.text}</Text>
                    </View>
                    }
                    ItemSeparatorComponent={this.renderSeparator}  
                />  
                </View>
               
             </View>

        );
    }

}

class Community extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text>Under working</Text>
        </View>
      );
    }
  }

  class general extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text>general page</Text>
        </View>
      );
    }
  }



const TabNavigator = createBottomTabNavigator({
    Tab_with_list: {
        
        screen:Tab_with_list,
        navigationOptions:{
            tabBarLabel:'Find Care',
            tabBarIcon:({tintColor})=>(
                <FontAwesome name='heartbeat' color={tintColor} size={24}/>
            )
            
        }

    },
    Community: {
        screen:Community,
        navigationOptions:{
            tabBarLabel:'Community',
            tabBarIcon:({tintColor})=>(
                <MaterialCommunityIcons name='file-document' color={tintColor} size={24}/>
            )
            
        }
    },
    general: {
        screen:general,
        navigationOptions:{
            tabBarLabel:'me',
            tabBarIcon:({tintColor})=>(
                <MaterialIcons name='person' color={tintColor} size={24}/>
            )
            
        }
    },
   
  },
  {
      initialRouteName:'Community',
    
      tabBarOptions:{
          activeTintColor:'red',
          inactiveTintColor:'gray',
          style: {
            backgroundColor: 'white',//color you want to change
            borderTopWidth:0
          }

      }

    } );
  
export default createAppContainer(TabNavigator);
  
  

const styles=StyleSheet.create({
container:{
   flex:1,
   margin:20,
   marginTop:10,
   marginBottom:0,
   
},

})