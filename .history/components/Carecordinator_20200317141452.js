import React, { Component } from 'react';
import { Text, View } from 'react-native';
import * as Font from 'expo-font';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';

export default class Carecordinator extends Component {


    componentDidMount() {
        Font.loadAsync({
          'open-sans-Regular': require('../assets/fonts/SourceSerifPro-Regular.ttf'),
        });
        
      }
        
  render() {
    return (
      <View style={{ flex: 1,}}>
        <Text style={{fontSize:heightPercentageToDP(3.1),alignSelf:'center',color:'#757171',fontFamily:'open-sans-Regular'}}>Don't see what you need ? </Text>
        <View style={{justifyContent:'center',}}><Text>You cre Advocate can help you</Text>
        <Text>Schedule an appointment or find the</Text>
        <Text>right provider.</Text></View>
      </View>
    );
  }
}