import React,{Component} from 'react'
import {View,Text,StyleSheet, Image,TouchableOpacity,Picker,TextInput,} from 'react-native'
import { Ionicons,FontAwesome,MaterialCommunityIcons} from '@expo/vector-icons'; // refer 
import RNPickerSelect from 'react-native-picker-select';
import { createAppContainer } from 'react-navigation';
import * as Font from 'expo-font';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { createStackNavigator } from 'react-navigation-stack';
import Welcome_to_profile from './Welcome_to_profile'
import { CheckBox } from 'react-native-elements'
import { ScrollView } from 'react-native-gesture-handler';

class Mental_health extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            checkbox1: false,
            checkbox2: false,
            checkbox3: false,
          }
        Font.loadAsync({
          'open-sans-Regular': require('../fonts/SourceSerifPro-Regular.ttf'),
        });
       
        
      }
    render(){
        return(
            <View style={styles.container}>
 



          
                 <View style={styles.image}>
                 <Image
                     
                     style={{position:'absolute',bottom:20,alignSelf:'center',    height:heightPercentageToDP(6),
                     width:widthPercentageToDP(14),
                     }}

                    source={require('../assets/logo(3).png')}
                    />
                    </View>
                    <View style={styles.text}>
                    <Text style={{position:'absolute',bottom:0, color:'#777E8B',alignSelf:'center',fontSize:24, fontFamily:'open-sans-Regular' ,}}>
                        TELL US ABOUT YOU
                    </Text>
                    </View>
                    <View style={styles.age_family}>
                    <View style={{backgroundColor:'white',borderWidth:1,borderColor:'silver',width:155,height:60,alignSelf:'center',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                         <MaterialCommunityIcons
                            name="human-male" size={33} color={'silver'}/>
                         <Text style={{color:'silver',marginLeft:10,fontSize:heightPercentageToDP(1.9)}}>Physical</Text>
                         
                    </View>
                    <View style={{backgroundColor:'#b2ddc1',borderWidth:1,borderColor:'#f7f7f7',width:155,height:60,alignSelf:'center',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                     <MaterialCommunityIcons style={{paddingLeft:15}} name="brain" size={32} color={'white'}/>
                     <Text style={{color:'white',marginLeft:10,fontSize:heightPercentageToDP(1.9)}}>Mental</Text>
                     <View style={styles.TriangleShapeCSS} />
                     </View>
                    </View>
 <ScrollView style={{backgroundColor:'red',height:350}}>

 <CheckBox
                    title='checkbox1'
          checked={this.state.checkbox1}
          onPress={() => this.setState({ checkbox1: !this.state.checkbox1 })}
        />

        <CheckBox
        title='checkbox2'
          checked={this.state.checkbox2}
          onPress={() => this.setState({ checkbox2: !this.state.checkbox2 })}
        />

        <CheckBox  
        title='checkbox2' 
          checked={this.state.checkbox3}
          onPress={() => this.setState({ checkbox3: !this.state.checkbox3 })}
          checkedColor={'red'}
        
        />

<CheckBox  
        title='checkbox2' 
          checked={this.state.checkbox3}
          onPress={() => this.setState({ checkbox3: !this.state.checkbox3 })}
          checkedColor={'red'}
        
        />
      
<CheckBox  
        title='checkbox2' 
          checked={this.state.checkbox3}
          onPress={() => this.setState({ checkbox3: !this.state.checkbox3 })}
          checkedColor={'red'}
        
        />
        <CheckBox  
                title='checkbox2' 
                  checked={this.state.checkbox3}
                  onPress={() => this.setState({ checkbox3: !this.state.checkbox3 })}
                  checkedColor={'red'}
                
                />
     </ScrollView>


                                      <View style={styles.button}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Welcome_to_profile')} style={{    padding: 5,
                         height: 100,
                         width: 100,  //The Width must be the same as the height
                         borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
                         borderColor:'silver',
                         borderWidth:1,
                         justifyContent:'center',
                         alignSelf:'center',
                         position:'absolute',
                         bottom:0,
                         marginBottom:10
                      
                         }}>
                        <Text style={{color:'silver',fontSize:20,textAlign:'center',}}>NEXT</Text>
                        </TouchableOpacity>
                    </View>

                   
                            </View>
        );
    }
}
const AppNavigator = createStackNavigator({
    Mental_health:{
        screen:Mental_health,
        navigationOptions: {
          header: null // Will hide header for all screens of current stack 
            
          }
                },
                             
     Welcome_to_profile:{
      screen:Welcome_to_profile,
     navigationOptions: {
       header: null // Will hide header for all screens of current stack 
         
       }
  
          },  
                   
          });
export default createAppContainer(AppNavigator);  

const styles=StyleSheet.create({
    container:{
        flex:1,
    },
    image:{
        flex:7,
    },
    text:{
        flex:3,
    },
    age_family:{
        flex:13,
        flexDirection:'row',
        justifyContent:'space-evenly',  
        backgroundColor:'blue'
       
    },

    describe_mental_health:{
        flexDirection:'row',
        flex:6,
        justifyContent:'center',
        alignItems:'center',
       
    },
    button:{
        flex:8
    },
    TriangleShapeCSS: {
 
        width: 0,
        height: 0,
        borderLeftWidth: 23.5,
        borderRightWidth: 23.5,
        borderBottomWidth: 20,
        borderStyle: 'solid',
        backgroundColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: '#b2ddc1',
        borderTopColor:'#b2ddc1',
        overflow:"hidden",
        position: 'relative',
        zIndex:1,
        top:38,
        right:52,
  
    
        
        transform: [
          {rotate: '180deg'}
        ]
      },

    
});
