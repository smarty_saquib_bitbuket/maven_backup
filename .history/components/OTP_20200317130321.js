
// https://github.com/Twotalltotems/react-native-otp-input  example for otpinput

import React,{Component} from 'react'
import {View,Text,Image,StyleSheet,TouchableOpacity} from 'react-native'
import OTPInputView from '@twotalltotems/react-native-otp-input' 
import { Ionicons,FontAwesome} from '@expo/vector-icons'; // refer 
import Self_with_age from './Self_with_age'
import * as Font from 'expo-font';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import IntroWelcome from '../componenet/IntroWelcome'
class OTP extends React.Component{
  constructor(props) {
    super(props)
    Font.loadAsync({
      'open-sans-Regular': require('../fonts/SourceSerifPro-Regular.ttf'),
    });
    
  }
    render(){
                return(
                <View style={styles.container}>
                  <View style={styles.subcontainer}>
                  <Image
                  source={require('../assets/logo(3).png')}
                  style={{
                    height:heightPercentageToDP(6),
                    width:widthPercentageToDP(14),marginBottom:20}}
                  />
                 <Text style={styles.text}>ENTER OTP</Text>
                <OTPInputView
                    style={{width: '90%', height: 110,marginTop:10}}
                    pinCount={6}
                    // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                    // onCodeChanged = {code => { this.setState({code})}}
                    autoFocusOnLoad
                    codeInputFieldStyle={styles.underlineStyleBase}
                    codeInputHighlightStyle={styles.underlineStyleHighLighted}
                    onCodeFilled = {(code => {
                        console.log(`Code is ${code}, you are good to go!`)
                    })}
                />
               <TouchableOpacity onPress={()=>this.props.navigation.navigate('IntroWelcome')} style={{padding:10,borderWidth:0.9,borderColor:'#f7f7f7',backgroundColor:'#b2ddc1',borderRadius:30,width:widthPercentageToDP(58),height:heightPercentageToDP(7.6),marginTop:30,maxWidth:300}}>
                           <Text style={{color:'white',fontSize:heightPercentageToDP(2.8),textAlign:'center'}}>Next</Text>
                           </TouchableOpacity>
                          
                     </View>
                     <View style={{flex:2}}>
                     
                       </View>
                     </View>
                   

                );

                    }
                }
 const AppNavigator = createStackNavigator({

                   
                 
                  
  OTP:{
         screen:OTP,
         navigationOptions: {
          header: null // Will hide header for all screens of current stack 
            
          }

             },
    Self_with_age:{
        screen:Self_with_age,
        navigationOptions: {
          header: null // Will hide header for all screens of current stack 
            
          }
           },
           IntroWelcome:{
            screen:IntroWelcome,
            navigationOptions: {
              header: null // Will hide header for all screens of current stack 
                
              }
               },
                 
                   
          });
                
                export default createAppContainer(AppNavigator);
              
              

const styles = StyleSheet.create({
  
    borderStyleBase: {
      width: 50,
      height: 45,

    },
  
    borderStyleHighLighted: {
      borderColor: "#03DAC6",
    },
  

    underlineStyleBase: {
      width: widthPercentageToDP(10),
      height: 44,
      borderWidth: 0,
      borderBottomWidth: 2.9,
      color:'#000000',fontWeight:'bold', textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#c4c4c4'



    },
  
    underlineStyleHighLighted: {
      borderColor: "#b2ddc1",
      fontSize:28,
      fontWeight:'bold',
      paddingBottom:12,
      
      
      
    },
    
    container:{
     
      flex:1,
     
    },
    subcontainer:{
      flex:2.4,
justifyContent:'center',
alignItems:'center',


    },
    text:{
      marginTop:25,
      fontSize:25,
      fontFamily:'open-sans-Regular' ,
      color:'#777E8B',
      
      
    }
    
  });
  