
//https://heartbeat.fritz.ai/building-an-app-introduction-slider-in-react-native-7a5139711157
import React,{ Component } from 'react'
import {View,Text,Image, StyleSheet,ScrollView,StatusBar} from 'react-native'
import { Provider as PaperProvider } from 'react-native-paper';
import { Button } from 'react-native-paper';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import LoginScreen from '../components/LoginScreen';
import { Dimensions } from 'react-native';
import Mental_health from './Mental_health'

import AppIntroSlider from 'react-native-app-intro-slider';
import { TouchableOpacity } from 'react-native-gesture-handler';
import NewAccount from './NewAccount'
 class HomeScreen extends Component {
    static navigationOptions = { header: null };
   
    componentDidMount() {          // remove default status bar 
        StatusBar.setHidden(true);
     }


    static navigationOptions = {
        header: null
    }
  constructor(props) {          //
    super(props);
    this.state = {

      show_App: false
      
    };
  }

  onDone = () => {
    this.setState({ show_App: true });
  };

  onSkip = () => {
    this.setState({ show_App: true });
  };
  render() {
    
                        return (
                            <View style={styles.container}>
                            <View style={{flex:8,backgroundColor:'pink'}}>
                                <AppIntroSlider
                                    slides={slides}
                                    onDone={this.onDone}
                                    showSkipButton={true}
                                    onSkip={this.onSkip}
                                    />
    
                            </View>  
                            <View style={{flex:3,flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}>
                            <TouchableOpacity  mode="outlined" onPress={() => this.props.navigation.navigate('LoginScreen')}  style={{width:120,height:64,borderWidth:2.3,borderColor:'#b2ddc1',borderRadius:5,justifyContent:'center',alignItems:'center'}}>
                            <Text style={{color:'#b2ddc1'}}> Signin</Text>
                    </TouchableOpacity>
                                            
                        <TouchableOpacity  mode="outlined" onPress={() => this.props.navigation.navigate('NewAccount')} style={{width:120,height:64,borderWidth:2.3,borderColor:'#b2ddc1',justifyContent:'center',alignItems:'center'}}>
                       <Text style={{color:'#b2ddc1'}}> Signup</Text>
                    </TouchableOpacity> 
                        
                        </View>
                        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                          <TouchableOpacity >
                            <Text style={{color:'silver'}}>Maye Clinic ®
                                Copyright Reserved 
                            </Text>
                            </TouchableOpacity>
                            </View>

                        
                    </View>
                        
        
      );
    }
  }


const AppNavigator = createStackNavigator({
  
    HomeScreen : {
      screen: HomeScreen,

    },
    LoginScreen:{
        screen:LoginScreen,
        navigationOptions: {
            header: null // Will hide header for all screens of current stack 
    
        }

        
    },
    Mental_health:{
      screen:Mental_health,
      navigationOptions:{
        header:null
      },
    },
   
    NewAccount:{
      screen:NewAccount,
      navigationOptions:{
        header:null
      },
    }

  });
  
  export default createAppContainer(AppNavigator);
const styles = StyleSheet.create({

  mainapp: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
  title: {
    fontSize: 26,
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
  },
  text: {
    color: '#fff',
    fontSize: 20,
  },
  image: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
    
  },
    container:{

        flex:1,
        flexDirection:'column',
     
     shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1.0

    },
});

const slides = [
  {
    key: 's1',
    title: 'Submit Your Application',
    text: 'We need your basic information for find someone ',
    image: {
      uri:
        'https://imgur.com/7ClQj9M.png',
    },
    titleStyle: styles.title,
    textStyle: styles.text,
    imageStyle: styles.image,
    backgroundColor: '#BCF4F5',
  },
  {
    key: 's2',
    title: ' Found match',
    text: 'Good new we Found someone who matching you',
    image: {
      uri:
        'https://imgur.com/BVQ79rh.png',
    },
    titleStyle: styles.title,
    textStyle: styles.text,
    imageStyle: styles.image,
    backgroundColor: '#B4EBCA',
  },
  {
    key: 's3',
    title: 'Just Dating ',
    text: 'let hangout and enjoy together with special place and special deal',
    image: {
      uri: 'https://imgur.com/RPI8wie.png',
    },
    titleStyle: styles.title,
    textStyle: styles.text,
    imageStyle: styles.image,
    backgroundColor: '#D9F2B4',
  },
  {
    key: 's4',
    title: 'Got new Love',
    text: ' Your not lonly anymore',
    image: {
      uri: 'https://imgur.com/f1GhQo1.png',
    },
    titleStyle: styles.title,
    textStyle: styles.text,
    imageStyle: styles.image,
    backgroundColor: '#FFB7C3',
  }
];