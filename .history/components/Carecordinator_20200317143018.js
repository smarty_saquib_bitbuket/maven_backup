import React, { Component } from 'react';
import { Text, View } from 'react-native';
import * as Font from 'expo-font';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';

export default class Carecordinator extends Component {


    componentDidMount() {
        Font.loadAsync({
          'open-sans-Regular': require('../assets/fonts/SourceSerifPro-Regular.ttf'),
        });
        
      }
        
  render() {
    return (
      <View style={{ flex: 1,backgroundColor:'white'}}>
      <View style={{height:heightPercentageToDP(40),justifyContent:"center",flex:0.4,alignItems:'center',backgroundColor:'red'}}><Text style={{fontSize:heightPercentageToDP(3.2),alignSelf:'center',color:'#757171',fontFamily:'open-sans-Regular',backgroundColor:'blue'}}>Don't see what you need ? </Text>
        <View style={{justifyContent:'center',alignItems:'center',padding:20}}><Text style={{fontSize:18}}>You Care Advocate can help you</Text>
        <Text style={{fontSize:18}}>Schedule an appointment or find the</Text>
        <Text style={{fontSize:18}}>right provider.</Text></View></View>
      </View>
    );
  }
}