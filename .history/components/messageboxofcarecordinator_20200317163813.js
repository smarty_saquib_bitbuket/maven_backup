import React, { Component } from 'react';
import { Text, View,StyleSheet,Image,TextInput} from 'react-native';
import * as Font from 'expo-font';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
export default class messageboxofcarecordinator extends Component {
  render() {
    return (
      <View style={styles.container}>
       <View style={styles.image}>
               
               <Image
               source={require('../assets/logo(3).png')}
               style={{position:'absolute',alignSelf:'center',height:heightPercentageToDP(6),
               width:widthPercentageToDP(14),}}
               />
         
           
          
             
           </View>
           <View style={styles.txt_view}>
                       <Text style={{position:'absolute',bottom:0,alignSelf:'center',fontSize:24,color:'#777E8B', fontFamily:'open-sans-Regular' ,}}>
                           TELL US ABOUT YOU
                       </Text>
                   </View>

                   <View style={styles.textAreaContainer} >
    <TextInput
      style={styles.textArea}
      underlineColorAndroid="transparent"
      placeholder="Type something"
      placeholderTextColor="grey"
      numberOfLines={10}
      multiline={true}
    />
  </View>



              
      </View>
    );
  }
}


const styles=StyleSheet.create({
    container:{
        flex:1,
        
    },
    image:{
        
        justifyContent:'center',
        alignItems:'center',
        height:180,
        backgroundColor:'red'
    
       
    },
    textAreaContainer: {
  
        borderWidth: 1,
        padding: 5
      },
      textArea: {
        height: 150,
        justifyContent: "flex-start"
      }

})