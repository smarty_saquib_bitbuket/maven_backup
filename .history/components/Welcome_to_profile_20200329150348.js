import React,{Component} from 'react'
import {View,Text,StyleSheet,Image,TouchableOpacity} from 'react-native'
import Card_with_doctors from './Card_with_doctors'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Clientdashboard from '../componenet/Clientdashboard'
import mainpage from '../componenet/mainpage'
class Welcome_to_profile extends React.Component{
render(){

    return(
        <View style={styles.container}>
                <View style={styles.image}>
                <Image source={require('../assets/ritikaamru.jpeg')} style={{borderRadius: heightPercentageToDP(18) / 2,
    height: heightPercentageToDP(18),
    width: heightPercentageToDP(18),borderWidth:3,borderColor:'#b2ddc1',}}/>
                   
               
                  
                </View>
                <View style={styles.welcome}>
                <Text style={{color:'silver',fontSize:20}}>
                       Hello Ritika 
                    </Text>
                    <Text style={{fontSize:25,marginTop:'5%',color:'#000000', textShadowOffset: {width: 2,height: 2},
                          textShadowRadius: 10,
                         textShadowColor:'#c4c4c4'}}>
                        Welcome!
                    </Text>
                </View>
              
                  
             
                <View style={styles.descriptions_with_button}>
                    <View style={{justifyContent:'center',alignItems:'center',position:'absolute',top:0,}}>
                        <Text style={{fontSize:18,color:'silver', textAlign:'center', padding:30, paddingTop:0}}>
                            You are all set and ready to be take care of.
                           We are happy to have you as a new member in our family.
                         Now lets move ahead.

                       
                       </Text>
                       </View>
                        <TouchableOpacity  style={{    padding: 5,
                         height: 100,
                         width: 100,  //The Width must be the same as the height
                         borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
                         borderColor:'silver',
                         borderWidth:1,
                         justifyContent:'center',
                         alignSelf:'center',
                         position:'absolute',
                         bottom:15,
                         marginBottom:10
                      
                         }} onPress={()=>this.props.navigation.navigate('mainpage')}>
                        <Text style={{color:'silver',fontSize:17,textAlign:'center',color:'#c4c4c4', textShadowOffset: {width: 2,height: 2},
                          textShadowRadius: 10,
                         textShadowColor:'#f8f8f8'}}>Get Started</Text>
                        </TouchableOpacity>
                </View>
        </View>
    );
}

}

const AppNavigator = createStackNavigator({
    Welcome_to_profile:{
        screen:Welcome_to_profile,
        navigationOptions: {
          header: null // Will hide header for all screens of current stack 
            
          }
                },
                             
    Clientdashboard:{
      screen:Clientdashboard,
     navigationOptions: {
       header: null // Will hide header for all screens of current stack 
         
       }
  
          },  
          mainpage:{
            screen:mainpage,
           navigationOptions: {
             header: null // Will hide header for all screens of current stack 
               
             }
        
                }, 
                   
          });
export default createAppContainer(AppNavigator);    

const styles=StyleSheet.create({
container:{
    flex:1,
    
},
image:{
    flex:4.5,
    justifyContent:'center',
    alignItems:'center',

   
},
welcome:{
flex:2.5,

justifyContent:'flex-start',
alignItems:'center',

},

descriptions_with_button:{
    flex:8,
     justifyContent:'center',
     alignItems:'center',
        textAlign:'center',

    
}

})