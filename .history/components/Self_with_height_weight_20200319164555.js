import React,{Component} from 'react'
import {View,Text,StyleSheet, Image,TouchableOpacity,Picker,} from 'react-native'
import { Ionicons,FontAwesome,MaterialCommunityIcons} from '@expo/vector-icons'; // refer 
import RNPickerSelect from 'react-native-picker-select';
import { TextInput } from 'react-native-paper';
import * as Font from 'expo-font';

import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import  Mental_health from './Mental_health'
import Tab_with_list from './Tab_with_list'


 class Self_with_height_weight extends React.Component{
    constructor(props) {
        super(props)
        Font.loadAsync({
          'open-sans-Regular': require('../fonts/SourceSerifPro-Regular.ttf'),
        });
        
      }
  
    render(){
        return(
            <View style={styles.container}>
                    <View style={styles.image}>
                    <Image
                     
                     style={{position:'absolute',bottom:20,alignSelf:'center',    height:heightPercentageToDP(6),
                     width:widthPercentageToDP(14),
                     }}

                    source={require('../assets/logo(3).png')}
                    />
                    </View>
                    <View style={styles.text}>
                    <Text style={{position:'absolute',bottom:0, color:'#777E8B',alignSelf:'center',fontSize:24, fontFamily:'open-sans-Regular'}}>
                        TELL US ABOUT YOU
                    </Text>
                    </View>
                    <View style={styles.age_family}>
                    <View style={{backgroundColor:'#9EDDCF',width:150,height:60,alignSelf:'center',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                         <MaterialCommunityIcons
                            name="human-male" size={27} color={'white'}/>
                         <Text style={{color:'white',marginLeft:10}}>Physical</Text>
                         
                    </View>
                    <View style={{backgroundColor:'white',borderWidth:0.98,borderColor:'#c4c4c4',width:150,height:60,alignSelf:'center',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                     <MaterialCommunityIcons name="brain" size={27} color={'silver'}/>
                     <Text style={{color:'silver',marginLeft:10}}>Mental</Text>
                     </View>
                    </View>
                    <View style={styles.status}>
               
                    <Text style={{marginLeft:8,fontSize:heightPercentageToDP(1.8)}}>Gender</Text>
                    <View style={{width:'100%',borderBottomWidth:1,borderColor:'silver',padding:2}}>
                    <RNPickerSelect
                  
                 onValueChange={(value) => console.log(value)}
                     items={[
                    { label: 'Male', value: 'male' },
                    { label: 'Female', value: 'female' },
                    { label: 'Other', value: 'other' },
                            ]}
                    />
                    </View>
                            <View style={{borderBottomWidth:1,borderBottomColor:'silver',marginTop:15,flexDirection:'row'}}>
                                <Text style={{marginLeft:8,fontSize:heightPercentageToDP(1.8)}}>Height</Text>

                                <TextInput

                             style={{backgroundColor:'white',width:'15%',padding:4,bottom:10,}}
                                />
                             <Text style={{marginTop:'10%',marginLeft:10}}>FT</Text>
                             <TextInput

                                    style={{backgroundColor:'white',width:'15%',marginTop:'2%',bottom:10,marginLeft:10}}
                                    />
                                    <Text style={{marginTop:'10%',marginLeft:10}}>IN</Text>

                            </View>
                        

                        <View style={{marginTop:15}}>
                        <Text style={{marginLeft:8,fontSize:heightPercentageToDP(1.8),padding:4,color:'#2C3335', textShadowOffset: {width: 1,height: 1},
      textShadowRadius: 3,
      textShadowColor:'#c4c4c4'}}>weight</Text>

                        <TextInput
                        style={{backgroundColor:'white',width:'100%',}}
                                    label='Enter your weight in kilograms'
                                />
                            </View>    
                    </View>
                    <View style={styles.button}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Mental_health')} style={{    padding: 5,
                         height: 100,
                         width: 100,  //The Width must be the same as the height
                         borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
                         borderColor:'#777E8B',
                         borderWidth:1,
                         justifyContent:'center',
                         alignSelf:'center'
                      
                         }}>
                        <Text style={{color:'#777E8B',fontSize:20,textAlign:'center',}}>NEXT</Text>
                        </TouchableOpacity>
                    </View>

            </View>
        );
    }
}

const AppNavigator = createStackNavigator({
    Self_with_height_weight:{
        screen:Self_with_height_weight,
        navigationOptions: {
          header: null // Will hide header for all screens of current stack 
            
          }
                },
                             
    Mental_health :{
      screen:Mental_health,
     navigationOptions: {
       header: null // Will hide header for all screens of current stack 
         
       }
  
          },  
                   
          });
export default createAppContainer(AppNavigator);    
const styles=StyleSheet.create({
    container:{
        flex:1,
    },
    image:{
        flex:2.3,
    },
    text:{
        flex:1,
    },
    age_family:{
        flex:4,
        flexDirection:'row',
        justifyContent:'space-evenly',  
       
    },
    status:{
        flex:6,
        margin:24,
        marginTop:0,
        
    },
    button:{
        flex:3,
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: 'purple',
        borderRadius: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
      },

    
});
