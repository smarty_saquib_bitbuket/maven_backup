import React,{Component} from 'react'
import {View,Text,StyleSheet, Image,TouchableOpacity,Picker,TextInput,} from 'react-native'
import { Ionicons,FontAwesome,MaterialCommunityIcons} from '@expo/vector-icons'; // refer 
import RNPickerSelect from 'react-native-picker-select';
import { createAppContainer } from 'react-navigation';
import * as Font from 'expo-font';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { createStackNavigator } from 'react-navigation-stack';
import Welcome_to_profile from './Welcome_to_profile'
import { CheckBox } from 'react-native-elements'
import { ScrollView } from 'react-native-gesture-handler';

class Mental_health extends React.Component{

    constructor(props) {
        super(props)
        this.state = {
            checkbox1: false,
            checkbox2: false,
            checkbox3: false,
          }
        Font.loadAsync({
          'open-sans-Regular': require('../fonts/SourceSerifPro-Regular.ttf'),
        });
       
        
      }
    render(){
        return(
            <View style={styles.container}>
 



          
                 <View style={styles.image}>
                 <Image
                     
                     style={{position:'absolute',bottom:20,alignSelf:'center',    height:heightPercentageToDP(6),
                     width:widthPercentageToDP(14),
                     }}

                    source={require('../assets/logo(3).png')}
                    />
                    </View>
                    <View style={styles.text}>
                    <Text style={{position:'absolute',bottom:0, color:'#777E8B',alignSelf:'center',fontSize:24, fontFamily:'open-sans-Regular' ,}}>
                        TELL US ABOUT YOU
                    </Text>
                    </View>
                    <View style={styles.age_family}>
                    <View style={{backgroundColor:'white',borderWidth:1,borderColor:'silver',width:155,height:60,alignSelf:'center',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                         <MaterialCommunityIcons
                            name="human-male" size={33} color={'silver'}/>
                         <Text style={{color:'silver',marginLeft:10,fontSize:heightPercentageToDP(1.9)}}>Physical</Text>
                         
                    </View>
                    <View style={{backgroundColor:'red',borderWidth:1,borderColor:'#f7f7f7',width:155,height:60,alignSelf:'center',}}>

                    </View>
 <ScrollView style={{backgroundColor:'white',height:350}}>

 <CheckBox
                    title='Malaria'
          checked={this.state.checkbox1}
          onPress={() => this.setState({ checkbox1: !this.state.checkbox1 })}
          checkedColor={'#b2ddc1'}
          
        />

                    <CheckBox  
        
        title='fever'
          checked={this.state.checkbox2}
          onPress={() => this.setState({ checkbox2: !this.state.checkbox2 })}
          checkedColor={'#b2ddc1'}
          
        />

        <CheckBox  
        title='cold' 
          checked={this.state.checkbox3}
          onPress={() => this.setState({ checkbox3: !this.state.checkbox3 })}
          checkedColor={'#b2ddc1'}
        
        />

<CheckBox  
        title='corona' 
          checked={this.state.checkbox4}
          onPress={() => this.setState({ checkbox4: !this.state.checkbox4 })}
          checkedColor={'#b2ddc1'}
        
        />
      
<CheckBox  
        title='Cough' 
          checked={this.state.checkbox5}
          onPress={() => this.setState({ checkbox5: !this.state.checkbox5})}
          checkedColor={'#b2ddc1'}
        
        />
        <CheckBox  
                title='Headache' 
                  checked={this.state.checkbox6}
                  onPress={() => this.setState({ checkbox6: !this.state.checkbox6})}
                  checkedColor={'#b2ddc1'}
                
                />

        <CheckBox  
                title='Body pain' 
                  checked={this.state.checkbox7}
                  onPress={() => this.setState({ checkbox7: !this.state.checkbox7})}
                  checkedColor={'#b2ddc1'}
                
                />

        <CheckBox  
                title='Liver infection' 
                  checked={this.state.checkbox8}
                  onPress={() => this.setState({ checkbox8: !this.state.checkbox8})}
                  checkedColor={'#b2ddc1'}
                
                />

        <CheckBox  
                title='Kidney stone' 
                  checked={this.state.checkbox9}
                  onPress={() => this.setState({ checkbox9: !this.state.checkbox9})}
                  checkedColor={'#b2ddc1'}
                
                />

        <CheckBox  
                title='sour eyes' 
                  checked={this.state.checkbox10}
                  onPress={() => this.setState({ checkbox10: !this.state.checkbox10})}
                  checkedColor={'#b2ddc1'}
                
                />

        <CheckBox  
                title='skin burn' 
                  checked={this.state.checkbox11}
                  onPress={() => this.setState({ checkbox11: !this.state.checkbox11 })}
                  checkedColor={'#b2ddc1'}
                
                />
     </ScrollView>


                                      <View style={styles.button}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Welcome_to_profile')} style={{    padding: 5,
                         height: 100,
                         width: 100,  //The Width must be the same as the height
                         borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
                         borderColor:'silver',
                         borderWidth:1,
                         justifyContent:'center',
                         alignSelf:'center',
                         position:'absolute',
                         bottom:0,
                         marginBottom:10
                      
                         }}>
                        <Text style={{color:'silver',fontSize:20,textAlign:'center',}}>NEXT</Text>
                        </TouchableOpacity>
                    </View>

                   
                            </View>
        );
    }
}
const AppNavigator = createStackNavigator({
    Mental_health:{
        screen:Mental_health,
        navigationOptions: {
          header: null // Will hide header for all screens of current stack 
            
          }
                },
                             
     Welcome_to_profile:{
      screen:Welcome_to_profile,
     navigationOptions: {
       header: null // Will hide header for all screens of current stack 
         
       }
  
          },  
                   
          });
export default createAppContainer(AppNavigator);  

const styles=StyleSheet.create({
    container:{
        flex:1,
    },
    image:{
        flex:7,
    },
    text:{
        flex:3,
    },
    age_family:{
        flex:12,
        flexDirection:'row',
        justifyContent:'space-evenly',  
        
       
    },

    describe_mental_health:{
        flexDirection:'row',
        flex:6,
        justifyContent:'center',
        alignItems:'center',
       
    },
    button:{
        flex:8
    },
    TriangleShapeCSS: {
 
        width: 0,
        height: 0,
        borderLeftWidth: 23.5,
        borderRightWidth: 23.5,
        borderBottomWidth: 20,
        borderStyle: 'solid',
        backgroundColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: '#b2ddc1',
        borderTopColor:'#b2ddc1',
        overflow:"hidden",
        position: 'relative',
        zIndex:1,
        top:19,
        left:52,
  
    
        
        transform: [
          {rotate: '180deg'}
        ]
      },

    
});
