import React,{Component} from 'react'
import {View,Text,StyleSheet,Image, Slider,Dimensions,FlatList,FlatListItemSeparator,ItemSeparatorComponent} from 'react-native'
import { Ionicons,FontAwesome,MaterialCommunityIcons} from '@expo/vector-icons'; // refer 
import { TouchableOpacity } from 'react-native-gesture-handler';
import { RadioButton } from 'react-native-paper';
import { createAppContainer } from 'react-navigation';

import { createStackNavigator } from 'react-navigation-stack';
import Self_with_height_weight from './Self_with_height_weight'
import * as Font from 'expo-font';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
class Self_with_status extends React.Component{
  constructor(props) {
    super(props)
    Font.loadAsync({
      'open-sans-Regular': require('../fonts/SourceSerifPro-Regular.ttf'),
    });
    
  }
state={
    slideValue: 18,
    
}
    _renderItem = ({item}) => (
        <View style={styles.listItem}>
        <RadioGroup>
            <RadioButton value={'item1'} >
              <Text>{item.key1}</Text>
            </RadioButton>
    
            <RadioButton value={'item2'}>
              <Text>{item.key2}</Text>
            </RadioButton>
    
            <RadioButton value={'item3'}>
              <Text>{item.key3}</Text>
            </RadioButton>
          </RadioGroup>
        </View>
      );
    
    state = {
        checked: 'first',
      };


    
        render(){

            return(
                <View style={styles.container}>
                  <View style={{flexDirection:'row'}}>
    <RadioButton.Group
        onValueChange={value => this.setState({ value })}
        value={this.state.value}
      >
        <View>
          <Text>First</Text>
          <RadioButton value="first" />
        </View>
        <View>
          <Text>Second</Text>
          <RadioButton value="second" />
        </View>
      </Ra    <RadioButton.Group
        onValueChange={value => this.setState({ value })}
        value={this.state.value}
      >
        <View>
          <Text>First</Text>
          <RadioButton value="first" />
        </View>
        <View>
          <Text>Second</Text>
          <RadioButton value="second" />
        </View>
      </RadioButton.Group>dioButton.Group>
      </View>
                <View style={styles.img_view}>
                    <Image

                    style={{position:'absolute',alignSelf:'center', height:heightPercentageToDP(6),
                    width:widthPercentageToDP(14),bottom:28}}
                    source={require('../assets/logo(3).png')}
                    />
                </View>
                <View style={styles.txt_view}>
                    <Text style={{position:'absolute',bottom:15,   color:'#777E8B',alignSelf:'center',fontSize:24, fontFamily:'open-sans-Regular' ,}}>
                        TELL US ABOUT YOU
                    </Text>
                </View>
                <View style={styles.age_family}>
                    <View style={{backgroundColor:'white',borderWidth:0.98,borderColor:'#c4c4c4',width:155,height:60,alignSelf:'center',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                         <FontAwesome name="birthday-cake" size={26} color={'silver'}/>
                         <Text style={{color:'#c4c4c4',paddingLeft:20,fontSize:18}}>Age</Text>
                         
                    </View>
                    
                    <View style={{backgroundColor:'#b2ddc1',borderWidth:1,borderColor:'#f7f7f7',width:155,height:60,alignSelf:'center',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                     <MaterialCommunityIcons name="ring" size={37} color={'white'}  />
                     <Text style={{color:'white',marginLeft:10,fontSize:18}}>Family</Text>
                     <View style={styles.TriangleShapeCSS} />
                     </View>
                     
                </View>
                <View style={styles.age_scroll}>
              
                <View style={{borderWidth:0.9,borderColor:'#c4c4c4',borderRadius:15,height:heightPercentageToDP(32)}}>
               
            
        <RadioButton.Group
        onValueChange={value => this.setState({ value })}
        value={this.state.value}
                 >
        <View style={styles.boxitem}>
        <RadioButton  value="first" style={{backgroundColor:'red'}} />

          <Text style={styles.textleftboxiteminner}>Single</Text>
        </View>












        
        <View style={styles.boxitem}>
        <RadioButton  value="second" />

          <Text style={styles.textleftboxiteminner}>Married</Text>
        </View>
        <View style={styles.boxitem}>
        <RadioButton  value="third" />

          <Text style={styles.textleftboxiteminner}>Married with 1 child</Text>
        </View>
        <View style={{padding:10,
  flex:1,
  alignItems:'center',flexDirection:"row"}}>
        <RadioButton  value="fourth" />

          <Text style={{paddingLeft:10,fontSize:17,color:'#2C3335', textShadowOffset: {width: 1,height: 1},
      textShadowRadius: 3,
      textShadowColor:'#c4c4c4'
}}>Married with 2 and more
           child</Text>
        </View>
     
     
      </RadioButton.Group>

                </View>
      
        
                </View>
              
                <View style={styles.button}>
                  <TouchableOpacity onPress={()=>this.props.navigation.navigate('Self_with_height_weight')} style={{    padding: 5,
                         height: 100,
                         width: 100,  //The Width must be the same as the height
                         borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
                         borderColor:'#777E8B',
                         borderWidth:1.5,
                         justifyContent:'center',
                         marginBottom:-25
                         
                         }}>
                        <Text style={{color:'#777E8B',fontSize:20,textAlign:'center',}}>NEXT</Text>
                        </TouchableOpacity>
                </View>
             </View>

            );
        }

}
const AppNavigator = createStackNavigator({
  Self_with_status:{
      screen:Self_with_status,
      navigationOptions: {
        header: null // Will hide header for all screens of current stack 
          
        }
              },
                           
    Self_with_height_weight:{
    screen:Self_with_height_weight,
    navigationOptions: {
     header: null // Will hide header for all screens of current stack 
       
     }

        },  
                 
        });
export default createAppContainer(AppNavigator);           
const styles=StyleSheet.create({
    container:{
        flex:1,
     
    },
    img_view:{
        flex:1.5,
        justifyContent:'center',
        alignItems:'center',
       
        
    },
    txt_view:{
        flex:0.7,
    },
    age_family:{
        flex:1.4,
        flexDirection:'row',
        justifyContent:'space-evenly',  
      
       margin:13,

    },
    age_scroll:{
        flex:3,
        margin:30,
        paddingTop:20,
    
        
    
    },
boxitem:{
  flexDirection:'row',
  borderWidth:0,
  borderBottomWidth:0.9,
  borderBottomColor:'#c4c4c4',
  padding:10,
  flex:1,
  alignItems:'center',
},
textleftboxiteminner:{
  paddingLeft:10,
  fontSize:17,
  color:'#000000c9',
   textShadowOffset: {width: 1,height: 1},
      textShadowRadius: 5,
      textShadowColor:'#c4c4c4'

},
    button:{
        flex:2,
        justifyContent:'center',
        alignItems:'center',
      
        flexDirection:'row',

        

    },
    TriangleShapeCSS: {
 
      width: 0,
      height: 0,
      borderLeftWidth: 23.5,
      borderRightWidth: 23.5,
      borderBottomWidth: 20,
      borderStyle: 'solid',
      backgroundColor: 'transparent',
      borderLeftColor: 'transparent',
      borderRightColor: 'transparent',
      borderBottomColor: '#b2ddc1',
      borderTopColor:'#b2ddc1',
      overflow:"hidden",
      position: 'relative',
      zIndex:1,
      top:38,
      right:52,

      flex:1,
      justifyContent:'center',
      alignItems:'center',
      transform: [
        {rotate: '180deg'}
      ]
    },
   
    
    })