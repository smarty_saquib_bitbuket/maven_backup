import React,{Component} from 'react'
import {View,Text,StyleSheet,Image,TouchableOpacity,SafeAreaView} from 'react-native'
import { Ionicons,FontAwesome,MaterialCommunityIcons} from '@expo/vector-icons'; // refer 
import * as Animatable from "react-native-animatable";

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Report from '../component/Report'
 export default class Paynow extends React.Component{

    render(){
            return(
                <View style={styles.container}>
                    <View style={styles.image}>
                        <View style={{flex:1,}}>
                        <Image

                        style={{ height: 110,
                            width: 110,  //The Width must be the same as the height
                            borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
                            borderColor:'silver',
                            borderWidth:1,
                         }}
                        source={require('../assets/fruite.jpeg')}
                        />
                        </View>
                     
                        <View style={{flex:1.6,}}>
                            <Text style={{fontSize:20,}}>Erika Mars</Text>
                            <Text style={{fontSize:20,color:'#B2DDC1'}}>Lactation Expert</Text>


                            </View>
                        </View>
                 <View style={styles.card_details}>
                            <View style={{flex:1,justifyContent:'center'}}>
                            <Text style={{color:'white',fontSize:18}}>A 10 Minute Video Call</Text>
                            </View>
                            
                            <Animatable.View style={{flex:1,width:'100%',alignItems:'center',alignItems:'center'}} animation="slideInDown" iterationCount={9} direction="alternate">

                         <View style={{
                             justifyContent:'space-evenly',
                             alignItems:'center',

                             borderRadius:15,
                       flex:1,
                       width:'80%',
                    backgroundColor:'#DAE0E2',
                      shadowColor: "#000",
                      shadowOffset: {
                          width: 0,
                          height: 6,
                      },
                      shadowOpacity: 0.39,
                      shadowRadius: 8.30, 
                      elevation: 13,
                    

                            }}>
                             <Text style={{fontSize:20,color:'white'}}>
                                 Friday,October  08
                                </Text>
                                <Text style={{fontSize:20,color:'white'}}>
                                08:30 pm -08:40 pm
                                </Text>
                            </View>
                            </Animatable.View>


                           <View style={{flex:1,justifyContent:'center'}}>
                            
                            <Text style={{fontSize:30,color:'white'}}><FontAwesome name='rupee' size={30} color={'white'}/>500</Text>
                            </View>
                            <View style={{flex:1,width:'80%',justifyContent:'center'}}>
                            <TouchableOpacity style={{padding:10,backgroundColor:'white',borderRadius:30}}>
                        <Text style={{color:'balck',fontSize:18,textAlign:'center'}}>Pay Now </Text>
                    </TouchableOpacity>
                            </View>
                 </View>
                    
                </View>

            );

    }
}
// const AppNavigator = createStackNavigator({
//     Paynow:{
//         screen:Paynow,
//         navigationOptions: {
//           header: null // Will hide header for all screens of current stack 
            
//           }
//                 },
                             
//      Myhistory:{
//       screen:Myhistory,
//      navigationOptions: {
//        header: null // Will hide header for all screens of current stack 
         
//        }
  
//           },  
                   
//           });
// export default createAppContainer(AppNavigator);    

const styles=StyleSheet.create({
    container:{
        flex:1,
        padding:20,
        marginBottom:0
    },
    image:{
        flex:1,
        flexDirection:'row',
        
        alignItems:'center',
    },
    card_details:{
        flex:2.5,
        backgroundColor:'#B2DDC1',
        justifyContent:'space-around',
        alignItems:'center',
        borderRadius:10,
        marginBottom:20        
    }
})