import React, { Component } from 'react';
import { Text, View ,ImageBackground,StyleSheet,TouchableOpacity,Image} from 'react-native';

import * as Font from 'expo-font';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import messageboxofcarecordinator from '../components/messageboxofcarecordinator'
export default class Carecordinator extends Component {


    componentDidMount() {
        Font.loadAsync({
          'open-sans-Regular': require('../assets/fonts/SourceSerifPro-Regular.ttf'),

        });
        
      }
        
  render() {
    return (
      <View style={{ flex: 1,backgroundColor:'white'}}>
      <View style={{justifyContent:"center",flex:1,alignItems:'center'}}><Text style={{fontSize:heightPercentageToDP(3.2),alignSelf:'center',color:'#757171',fontFamily:'open-sans-Regular',}}>Don't see what you need ? </Text>
        <View style={{justifyContent:'center',paddingTop:40}}><Text style={{fontSize:16}}>You Care Advocate can help you</Text>
        <Text style={{fontSize:16}}>Schedule an appointment or find the</Text>
        <Text style={{fontSize:16}}>right provider.</Text></View>
        <View style={{marginTop:40,paddingBottom:20}} ><Image
                      
                      style={{ height: 80,
                        
                         width: 80,  //The Width must be the same as the height
                         borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
                         borderColor:'#b2ddc1',
                         borderWidth:2,
                         alignSelf:"center"
                         
                      }}
                     source={require('../assets/ritikaamru.jpeg')}
                     /></View>

<TouchableOpacity style={{    padding: 5,
                         height: 40,
                         width: 170,  //The Width must be the same as the height
                         borderRadius:50, //Then Make the Border Radius twice the size of width or Height   
                         borderColor:'#f7f7f7',
                         borderWidth:1,
                         backgroundColor:'#b2ddc1',
                         justifyContent:'center',
                         alignSelf:'center',
                         position:'relative',
                         }} onPress={()=>this.props.navigation.navigate('messageboxofcarecordinator')}>
                        <Text style={{color:'#ffffff',fontSize:15.5,textAlign:'center',}}>Message Kaithlyn</Text>
                        </TouchableOpacity>
        </View>
       
      </View>
    );
  }
}

const AppNavigator = createStackNavigator({
    Carecordinator:{
        screen:Carecordinator,
        navigationOptions: {
          header: null // Will hide header for all screens of current stack 
            
          }
                },
                             
    messageboxofcarecordinator:{
      screen:messageboxofcarecordinator,
     navigationOptions: {
       header: null // Will hide header for all screens of current stack 
         
       }
  
          },  
     
          });
export default createAppContainer(AppNavigator);    

