import React,{Component} from 'react'
import {View,Text,StyleSheet,Image,TouchableOpacity} from 'react-native'
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Ionicons,FontAwesome,MaterialCommunityIcons,MaterialIcons,AntDesign,Entypo} from '@expo/vector-icons'; // refer 
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';

export default class  Report extends React.Component{
  
    render(){
        return(
            <View style={styles.container}>
<ScrollView>

<View style={styles.Appointment}>


<View style={styles.Appointmentcontent}>

<View style={styles.Appoinmentitem} >

<View style={{flex:1,justifyContent:'space-around',margin:5,}}>

<View ><Text style={{fontSize:20,padding:10,color:'#c4c4c4',fontWeight:'bold', textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#f8f8f8'}} >Appoinment No <Text style={{fontSize:20,padding:10,color:'#b2ddc1',fontWeight:'bold', textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#f7f7f7'}}> #AB34RT</Text></Text></View>

<View ><Text style={{fontSize:20,padding:10,color:'#c4c4c4',fontWeight:'bold'}} >Date <Text style={{fontSize:22,padding:10,color:'#000000',fontWeight:'bold', textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#c4c4c4'  }}>  Thursday ,24th Nov</Text></Text></View>

<View ><Text style={{fontSize:20,padding:10,color:'#c4c4c4',fontWeight:'bold'}} >Time<Text style={{fontSize:20,padding:10,color:'#000000',fontWeight:'bold', textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#c4c4c4'}} >   3:30 pm</Text></Text></View>

</View>

</View>

</View>


</View>
 
                
                
                
                
                
                <View><Text style={{fontSize:15,color:'#c4c4c4',paddingLeft:25}}>consulted By </Text></View>

                       
                <View style={styles.subcontainer}>
                   
                    <View style={styles.image}>
                        



                        
                        <View style={{flex:1,justifyContent:'center', }}>
                       
                        <Image style={{borderRadius: heightPercentageToDP(12) / 2,
height: heightPercentageToDP(12),
width: heightPercentageToDP(12),borderWidth:3,borderColor:'#9eddcf'}}
                        source={require('../assets/ritikaamru.jpeg')}
                        />
                        </View>
                        
                    </View>
             <View style={styles.text_with_button}>
                 <View style={{flex:2,justifyContent:"center",flexDirection:'column'}}>
                     
                         <Text style={{color:'#000000',fontSize:19,padding:4}}>Dr. Dean koontz</Text>

                          
                        </View>

                    </View>
                  
                </View>
                <View><Text style={{fontSize:16,padding:10, textAlign:'center',color:'#c4c4c4'}}>REPORTS</Text></View>
                <View style={{flex:3, margin:25,backgroundColor:'white',}}>
                  <ScrollView>
                    <View style={{flex:1,justifyContent:'space-evenly'}}>
                        <Text style={{color:'#9eddcf', fontSize:16,paddingTop:10}}>Current Symptoms</Text>
                           <Text style={{textAlign:"left",color:'#c4c4c4',fontSize:13,paddingTop:10}}>
                              It is a long established fact that a reader will be
                               distracted by the readable content of a page when looking it's layout
                                </Text>
                                </View>
                             <View style={{flex:1,justifyContent:'space-evenly',paddingTop:10}}>
                        <Text  style={{color:'#9eddcf',fontSize:16,paddingTop:10}}>History</Text>
                 <Text style={{textAlign:'left',color:'#c4c4c4',fontSize:13,paddingTop:10}}>  
             It is a long established fact that a reader will be distracted by the readable content of a page when 
             looking it's layout
         </Text>
             </View>
           
                        <View style={{flex:1,justifyContent:'space-evenly'}}>
                        <Text  style={{color:'#9eddcf',fontSize:16,paddingTop:10}}>Diagnosis and Treatment</Text>
                        <Text style={{color:'#c4c4c4',fontSize:13,paddingTop:10}}>It is a long established fact that a reader will be distracted by the readable content of a page when looking it's layout
                            </Text>
                        </View>
                        <View style={{flex:1,justifyContent:'space-evenly'}}>
                        
                        </View>
                        </ScrollView>
                </View>
                </ScrollView>
            </View>
        );
    }
}



const styles=StyleSheet.create({
container:{
    backgroundColor:'white',
    flex:1,
},
subcontainer:{
    height:'15%',
    width:'100%',
    paddingLeft:22,
 
    flexDirection:'row',
    justifyContent:'space-evenly' ,
  
   
 
  
    
},
image:{
    flex:2,
    
    flexDirection:'row',
    justifyContent:'center',
  

},
text_with_button:{
    flex:4.4,
    
},


Appointment:{
    height:heightPercentageToDP(31),
    width:'100%',
    paddingTop:20,

    

},

Appointmentcontent:{
  flex:1,
  flexDirection:'row',
  
  margin:14,
  justifyContent:'center',
  alignItems:'center',

},
Appoinmentitem:{
  borderWidth:1,
  height:heightPercentageToDP(21),
  width:widthPercentageToDP(90), 
  borderRadius:5,
  borderColor:'#c4c4c4',
}

})