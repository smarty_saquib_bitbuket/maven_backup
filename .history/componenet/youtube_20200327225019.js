import React, { Component } from 'react';
import { Text, View } from 'react-native';
import {WebView} from 'react-native-webview'

export default class HelloWorldApp extends Component {
  render() {
    return (
      <WebView
      originWhitelist={['*']}
      source={{ uri: 'https://docs.expo.io/versions/latest/sdk/webview/' }}
      style={{ marginTop: 20 }}
    />
    );
  }
}
