import React, { Component } from 'react';
import { Text, View } from 'react-native';
import {WebView} from 'react-native-webview'

export default class HelloWorldApp extends Component {
  render() {
    return (
      <WebView
      originWhitelist={['*']}
      source={{ html: '<h1>Hello world</h1>' }}
      style={{ marginTop: 20 }}
    />
    );
  }
}
