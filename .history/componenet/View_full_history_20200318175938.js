import React,{Component} from 'react'
import {View,Text,StyleSheet,TouchableOpacity,Image, Alert} from 'react-native'
import { Ionicons,FontAwesome,MaterialCommunityIcons} from '@expo/vector-icons'; // refer 
import Nukkad from './Nukkad'

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
 class View_full_history extends React.Component{
render(){
    const { navigate } = this.props.navigation;
    return(
            
            <View style={styles.container}>
               <View style={styles.image}>
                 <View>
                 <Image
                      
                style={{ height: 125,

                width: 125,  //The Width must be the same as the height
                borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
                borderColor:'#b2ddc1',
                borderWidth:2,
                alignSelf:"center"

                }}
                source={require('../assets/ritikaamru.jpeg')}
                />
                </View>

                      
                        <View style={{marginLeft:'7%'}}>
                            <Text style={{fontSize:22,}}>Ritika Amru</Text>
                             <Text style={{fontSize:16,color:"#c4c4c4",marginTop:2}}>26 years old</Text>    
                               </View>
                                 </View>
                         
          <View style={styles.box}>

              <View ><Text style={{fontSize:19,color:'#c4c4c4',paddingLeft:20}} >Date <Text style={{fontSize:21,padding:10,color:'#000000', textShadowOffset: {width: 2,height: 2},
                 textShadowRadius: 10,
                    textShadowColor:'#c4c4c4'  }}> Thursday ,24th Nov</Text></Text></View>

                       <View ><Text style={{fontSize:20,paddingLeft:20,color:'#c4c4c4',}} >Time<Text style={{fontSize:20,padding:10,color:'#000000', textShadowOffset: {width: 2,height: 2},
                          textShadowRadius: 10,
                         textShadowColor:'#c4c4c4'}} >   3:30 pm</Text></Text></View>

                            <View>
                            <TouchableOpacity  style={{padding:9,borderWidth:1,borderColor:'#f8f8f8',backgroundColor:'#9fddd0',borderRadius:30 ,justifyContent:'center',width:'80%',alignSelf:'center'}}>


                                <View style={{flexDirection:'row',}}>
                             <Text style={{flex:2.5,textAlign:'center',fontSize:18,color:'#ffffff'}}>2 Days</Text>
                        <View
                     style={{
                 borderLeftWidth: 1,
                borderLeftColor: 'white',
             }}
          />
                           
     <Text style={{flex:3,fontSize:18,textAlign:'center',color:'#ffffff'}}>2 Hours</Text>
         <View
            style={{
            borderLeftWidth: 1,
            borderLeftColor: 'white',
            }}
            />
              <Text style={{flex:3,fontSize:18,textAlign:'center',color:'#ffffff'}}>38 mins</Text>

                </View>
                    </TouchableOpacity>

                      </View>
</View>


<View style={styles.paragraph}>
<Text style={{fontSize:16,color:'silver'}}>It is a long established fact that a reader
will be distracted by the readable
content of a page when looking at its
layout. The point of using Lorem Ipsum
is that it has a more-or-less normal
distribution of letters, as opposed to
using 'Content here, content here',
making it look like readable English.</Text>
</View>
<View style={styles.View_full_history}>
<Text style={{fontSize:18,color:'#3BB9FF',alignSelf:'center'}}>View Full History</Text>
</View>
<View style={styles.button}>
<TouchableOpacity    onPress={() =>this.props.navigation.navigate('Nukkad')}
 style={{    padding: 5,
height: 100,
width: 100,  //The Width must be the same as the height
borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
borderColor:'silver',
borderWidth:1,
justifyContent:'center',
alignSelf:'center',
backgroundColor:'silver'

}} >
<View style={{justifyContent:'center',alignItems:'center'}}>
<Ionicons name='md-call' size={50} color={'white'}/>
</View>
</TouchableOpacity>
</View>
</View>

         );

        }

    }

    const AppNavigator = createStackNavigator({
        View_full_history:{
            screen:View_full_history,
            
                    },
                                 
                    Nukkad:{
          screen:Nukkad,
         navigationOptions: {
           header: null,
           // Will hide header for all screens of current stack   
           headerStyle: {
            backgroundColor: 'white',
            borderWidth:0,
            borderBottomWidth:0
        }

         },
         
           },
            
            
      
                       
              });
    export default createAppContainer(AppNavigator);  
const styles=StyleSheet.create({
    container:{
        flex:1,
        padding:20,
        paddingBottom:0,
        
        backgroundColor:'white',
        
    },
    image:{
    flex:2.5,
    flexDirection:'row',
    
    alignItems:'center', 
},

box:{
    flex:2.2,
    borderWidth:0.9,
    borderColor:'#c4c4c4',
    borderRadius:7,
    
    justifyContent:'space-around',
},
paragraph:{
    flex:2.8,
    justifyContent:'center'
},
View_full_history:{
    flex:0.8,
  
},
button:{
    flex:2,

}

})