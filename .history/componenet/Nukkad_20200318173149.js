import React, { Component } from 'react';
import { Text, View ,ImageBackground,StyleSheet,TouchableOpacity} from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Ionicons,FontAwesome,MaterialCommunityIcons} from '@expo/vector-icons'; 
class Nukkad extends Component {
  render() {
    const { navigate } = this.props.navigation;

    return (
      
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ImageBackground  source={require('../assets/ritikaamru.jpeg')}
  style={{width: '100%', height: '100%'}}>
    <View style={styles.button}>
                        <TouchableOpacity  style={{    padding: 5,
                         height: 100,
                         width: 100,  //The Width must be the same as the height
                         borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
                         borderColor:'silver',
                         borderWidth:1,
                         justifyContent:'center',
                         alignSelf:'center',
                         backgroundColor:'red'
                      
                         }}>
                             <View style={{justifyContent:'center',alignItems:'center'}}>
                        <Ionicons name='md-call' size={50} color={'white'}/>
                        </View>
                        </TouchableOpacity>
                        </View>
  </ImageBackground>
      </View>
    );
  }
}
const AppNavigator = createStackNavigator({
  Nukkad:{
      screen:Nukkad,
     
        },
        
        navigationOptions:()=>{
          return {
            tabBarVisible:false,
          }
          },
                           
       
                 
        });
export default createAppContainer(AppNavigator);  
const styles=StyleSheet.create({

    button:{
        flex:5,
        justifyContent:'flex-end',
        alignItems:'center'
    
    }
    
    })