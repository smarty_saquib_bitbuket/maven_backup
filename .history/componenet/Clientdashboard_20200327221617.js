import React,{Component} from 'react'
import {View,Text,StyleSheet, Image,ScrollView,TouchableOpacity} from 'react-native'
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import { Ionicons,FontAwesome,MaterialCommunityIcons,MaterialIcons,Foundation,AntDesign,Feather} from '@expo/vector-icons'; // refer 
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { LinearGradient } from 'expo-linear-gradient';
import Viewpasthistory from './Viewpasthistory'
import mycareteaminside from '../componenet/mycareteaminside'

 class Clientdashboard extends React.Component{

    render(){
            const { navigate } = this.props.navigation;

        return(
            <View style={styles.container}>
                   
     
                   <View style={styles.image}>
                   <LinearGradient
          colors={['#b2ddc1', 'transparent']}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            height: 200,
          }}
        />
                        <View style={{flex:1,paddingTop:50}}>
                        <Image

                        style={{borderRadius: heightPercentageToDP(15.7) / 2,
                            height: heightPercentageToDP(15.5),
                            width: heightPercentageToDP(15.5),
                             //The Width must be the same as the height
                          //Then Make the Border Radius twice the size of width or Height   
                           
                            borderWidth:3,
                            borderColor:'#f7f7f7',
                            alignSelf:"center",
                            
                        
                            
                         }}
                        source={require('../assets/ritikaamru.jpeg')}
                        />
                        </View>
                     
                        <View style={{flex:1.3,paddingTop:40,}}>
                            <Text style={{fontSize:heightPercentageToDP(3.1),color:'white',shadowColor:'red'}}>RITIKA AMRU</Text>


                            </View>

                        </View>


                        <View style={{flex:4,padding:27,}}>
                        <View style={styles.box}>

                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Viewpasthistory')} style={{flex:0.8,borderWidth:0.9,borderColor:'#c4c4c4',backgroundColor:'',marginRight:'7%',borderRadius:7}}>
                       <View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'flex-end'}}>
                        <MaterialIcons name='message' size={heightPercentageToDP(5)}/> 
                        <MaterialCommunityIcons name='comment' size={heightPercentageToDP(3.5)} color={'#B2DDC1'}/>
                        </View>
                            <View style={{flex:1.5,alignItems:'center'}}>
                            <Foundation name='male-female' size={heightPercentageToDP(5)}/>
                            <Text style={{color:'#737380'}}>Appointments</Text>
                            </View>
                        </TouchableOpacity>
                      

                        <View style={{flex:0.8,borderWidth:0.9,borderColor:'#c4c4c4',backgroundColor:'white',borderRadius:7}}>
                            <View style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'center',alignItems:'flex-end'}}>
                           <View style={{flex:1}}>
                            <Image

                                    style={{ borderRadius: heightPercentageToDP(6.1) / 2,
                                        height: heightPercentageToDP(6.1),
                                        width: heightPercentageToDP(6.1), //Then Make the Border Radius twice the size of width or Height   
                                        borderColor:'#b2ddc1',
                                        borderWidth:3,
                                        alignSelf:"center",
                                        marginLeft:20,
                                        zIndex:1
                                        
                                    }}
                                    source={require('../assets/ritikaamru.jpeg')}
                                    />
                                    </View>
                                    <View style={{flex:1,}}>
                                    <Image

                                    style={{ borderRadius: heightPercentageToDP(6.9) / 2,
                                        height: heightPercentageToDP(6.9),
                                        width: heightPercentageToDP(6.9), //Then Make the Border Radius twice the size of width or Height   
                                        borderColor:'silver',
                                        borderWidth:1,
                                        alignSelf:"center",
                                        marginTop:30,
                                        marginRight:50,
                                        
                                        
                                    }}
                                    source={require('../assets/doctor.jpeg')}
                                    />
                                    </View>
                            </View>
                            <TouchableOpacity onPress={()=>this.props.navigation.navigate('mycareteaminside')}>
                            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                                <Text style={{color:'#737380'}}>
                                    My Care Team
                                </Text>
                            </View>
                            </TouchableOpacity>
                        </View>
                        </View>
                        <View style={styles.message_health_history}>
                            <View style={{flex:1,borderBottomWidth:0.9,borderColor:'#c4c4c4',justifyContent:'center'}}>
                               <View style={{flexDirection:'row',marginLeft:'5%'}}>
                               <AntDesign name='message1' size={heightPercentageToDP(3.9)} color={'#B2DDC1'}/>
                              
                               <Text style={{marginLeft:'5%',color:'#737380'}}>Messages</Text>
                          
                           
                               </View>
                             
                            </View>
                            <View style={{flex:1,justifyContent:'center'}}>
                            <View style={{flexDirection:'row',marginLeft:'5%'}}>
                               <MaterialCommunityIcons name='file-document-box-multiple-outline' size={heightPercentageToDP(3.9)} color={'#B2DDC1'}/>
                              
                               <Text style={{marginLeft:'5%',color:'#737380'}}>My Health History</Text>
                          
                           
                               </View>
                            </View>

                        </View>
                       
                        <View style={styles.APCL}>
                    
                              <View style={{height:60,maxHeight:60,borderBottomWidth:0.9,borderBottomColor:'#c4c4c4',justifyContent:'center'}}>
                              <Text style={{marginLeft:'5%',color:'#737380'}}>Account Settings</Text>
                       </View>
                       <View style={{height:60,maxHeight:60,borderBottomWidth:0.9,borderBottomColor:'#c4c4c4',justifyContent:'center'}}>
                      
                              
                              <Text style={{marginLeft:'5%',color:'#737380'}}>Payments  & Credits</Text>
                       </View>

                       <View style={{height:60,maxHeight:60,borderBottomWidth:0.9,borderBottomColor:'#c4c4c4',justifyContent:'center'}}>
                       
                              
                              <Text style={{marginLeft:'5%',color:'#737380'}}>Contact About</Text>
                       </View>
                       <View style={{height:60,maxHeight:60,borderBottomWidth:0.9,borderBottomColor:'#c4c4c4',flexDirection:'row'}}>
                       
                              
                      
                  <View style={{flex:1,}}>
                      
                      <View style={{flexDirection:'row',justifyContent:'space-between',flex:1,alignItems:'center'}}>
                 <View style={{flex:7}}>
                  <Text style={{marginLeft:'5%',color:'#737380'}}>Logout</Text>
                      
                     </View>
                      <View style={{flex:1}}>
                      <Ionicons name='md-power'  size={heightPercentageToDP(4.5) } color={'#b2ddc1'} /></View>
                      </View>
                      </View>
                </View>
                      
                             
                        

                        
                       
                       
                        </View>
                    
                        </View>
            </View>
        );
    }
}
const AppNavigator = createStackNavigator({
                           
    Clientdashboard:{
      screen:Clientdashboard,
     navigationOptions: {
       header: null // Will hide header for all screens of current stack 
         
       }
  
          }, 
          Viewpasthistory:{
            screen:Viewpasthistory,
            navigationOptions: {
              header: null // Will hide header for all screens of current stack 
                
              }
                    }, 
                    Viewpasthistory:{
                        screen:Viewpasthistory,
                        navigationOptions: {
                          header: null // Will hide header for all screens of current stack 
                            
                          }
                                }, 
                   
          });
export default createAppContainer(AppNavigator);    



const styles=StyleSheet.create({

    container:{
        flex:1,
        paddingBottom:0,
        backgroundColor:'white',

    },
    image:{
        flex:1.65,
        flexDirection:'row',
        backgroundColor:'#9eddcf',
        alignItems:'center', 
      
      
        
    },
    box:{
        
       height:'27%',
       maxHeight:'30%',
       minHeight:'20%',
        flexDirection:'row',  
            marginBottom:'5%',
            backgroundColor:'#ffffff'
       
        },
    message_health_history:{
       flex:0.4,
       
        backgroundColor:'white',
        borderWidth:0.9,
        borderRadius:5,
        borderColor:'#c4c4c4',
        
       
    
    },
    APCL:{
        flex:0.8,
        borderWidth:0.9,
        marginTop:'5%',
        borderRadius:5,
        borderColor:'#c4c4c4',
   
        justifyContent:'space-evenly'
        
    
       
    }

})