import React,{Component} from 'react'
import {View ,Text,StyleSheet,FlatList,Alert,Image,TouchableOpacity} from 'react-native'
import { Searchbar, IconButton } from 'react-native-paper';
import { Ionicons,FontAwesome,MaterialCommunityIcons,MaterialIcons,Entypo} from '@expo/vector-icons'; // refer 
import * as Font from 'expo-font';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Profile_with_details from './Profile_with_details '
import appoinmentfixdate from './appoinmentfixdate'

import { ScrollView } from 'react-native-gesture-handler';
import Report from '../components/Report'
class Viewreportinside extends React.Component{

  componentDidMount() {
    Font.loadAsync({
      'open-sans-Regular': require('../assets/fonts/SourceSerifPro-Regular.ttf'),
    });
    
  }
    
    render(){

        return(
            <View style={{ backgroundColor:'white',marginTop:0, }}>
         <ScrollView><View><Text style={{fontSize:30,paddingTop:20,textAlign:'center',color:'#757171',fontFamily:'open-sans-Regular' }}>Consultation</Text></View>
      

            <FlatList  
            data={[  {title:'Dr. Amaan Thakur',subtitle:'General Health',experience:'12 years of experience',avtar:require('../assets/doctor.jpeg'),
            },
        


            ]}  
          
            renderItem={({item}) =>  
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Report')} style={styles.card}>
                        <View style={{flexDirection:'row',margin:22,backgroundColor:'white'}}>
                        <View style={{flexDirection:'column-reverse',flex:2,}}>
                        
                        <View style={{flexDirection:"row",}}>
                            <Image
                    
                        style={{width:52,height:52,borderRadius:100,borderWidth:0.1,borderColor:'#000000',}}
                        source={item.avtar}
                        />
                        <View style={{paddingLeft:15,paddingBottom:5}}><Text style={{fontSize:18, textShadowOffset: {width: 2,height: 2},textShadowRadius: 10,textShadowColor:'#c4c4c4'}}>{item.title}</Text>
                        <Text style={{color:'#b2ddc1',paddingBottom:35}}>{item.subtitle}</Text>
                        </View></View>
                        </View>
                        
                        <View style={{flexDirection:'row',flex:1,justifyContent:'space-around',}}>
                            <View
                                style={{
                                
                                }}
                                />

                        
             
<View style={{flex:0.98,justifyContent:'flex-end',backgroundColor:'white',alignItems:'flex-end',height:100}}> 
  <TouchableOpacity onPress={()=>this.props.navigation.navigate('Profile_with_details')}>
                        
                        <Text style={{fontSize:13.5 ,color:'#c4c4c4',}}>17/03/2020</Text>
                        </TouchableOpacity>
                        </View>
                        </View>
                        </View>
                        
                       
                       </TouchableOpacity>
                     
                      }  
                    ItemSeparatorComponent={this.renderSeparator}  
           
        
            />  
            
               </ScrollView>
             </View>

        );
    }

}


const AppNavigator = createStackNavigator({
  Viewreportinside:{
      screen:Viewreportinside,
      navigationOptions: {
        header: null // Will hide header for all screens of current stack 
          
        }
              },
                           
  Report:{
    screen:Report,
   navigationOptions: {
     header: null // Will hide header for all screens of current stack 
       
     }

        },  
               
        });
export default createAppContainer(AppNavigator);    

// class Community extends React.Component {
//     render() {
//       return (
//         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//           <Text>Under working</Text>
//         </View>
//       );
//     }
//   }

//   class general extends React.Component {
//     render() {
//       return (
//         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//           <Text>general page</Text>
//         </View>
//       );
//     }
//   }



// const TabNavigator = createBottomTabNavigator({
//     E: {
        
//         screen:E,
//         navigationOptions:{
//             tabBarLabel:'Find Care',
//             tabBarIcon:({tintColor})=>(
//                 <FontAwesome name='heartbeat' color={tintColor} size={24}/>
//             )
            
//         }

//     },
//     Community: {
//         screen:Community,
//         navigationOptions:{
//             tabBarLabel:'Community',
//             tabBarIcon:({tintColor})=>(
//                 <MaterialCommunityIcons name='file-document' color={tintColor} size={24}/>
//             )
            
//         }
//     },
//     general: {
//         screen:general,
//         navigationOptions:{
//             tabBarLabel:'me',
//             tabBarIcon:({tintColor})=>(
//                 <MaterialIcons name='person' color={tintColor} size={24}/>
//             )
            
//         }
//     },
   
//   },
//   {
//       initialRouteName:'Community',
    
//       tabBarOptions:{
//           activeTintColor:'#B2ddc1',
//           inactiveTintColor:'gray',
//           style: {
//             backgroundColor: 'white',//color you want to change
//             borderTopWidth:0
//           }

//       }

//     } );
  
     
const styles=StyleSheet.create({
container:{
   flex:1,
backgroundColor:'white'
   
   
},
card:{
    
    flex:2,
    borderWidth:1,
    borderColor:'#f7f7f7',
    margin: 15,
    borderRadius:5,
  
    
    shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 4,
},
shadowOpacity: 0.32,
shadowRadius: 5.46,
backgroundColor:'white',
height:144,
elevation: 9,
borderRadius:20

}

})