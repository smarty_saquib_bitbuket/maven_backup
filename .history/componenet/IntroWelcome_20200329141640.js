import React,{Component} from 'react'
import {View,Text,StyleSheet,Image,TouchableOpacity, ImagePickerIOS} from 'react-native'


import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Clientdashboard from './Clientdashboard'
import Self_with_age from '../components/Self_with_age'
class IntroWelcome extends React.Component{
render(){

    return(
        <View style={styles.container}>
                <View style={styles.image}>
               
                <Image source={require('../assets/ritikaamru.jpeg')} style={{borderRadius: heightPercentageToDP(18.5) / 2,
    height: heightPercentageToDP(18.5),
    width: heightPercentageToDP(18.5),borderWidth:3,borderColor:'#b2ddc1',}}/>
                    <Text style={{marginTop:'5%',color:'#9EDDCF',fontSize:15}}>
                        + Add Picture
                    </Text>
               
                  
                </View>
                <View style={styles.welcome}>
                <Text style={{color:'silver',fontSize:20}}>
                        Hi Ritika
                    </Text>
                    <Text style={{fontSize:25,marginTop:'5%'}}>
                        Welcome To Maye !
                    </Text>
                </View>
              
                  
             
                <View style={styles.descriptions_with_button}>
                    <View style={{justifyContent:'center',alignItems:'center',position:'absolute',top:0,}}>
                        <Text style={{fontSize:18,color:'silver',padding:40,paddingTop:0}}>

                            Welcome to the Maye family {"\n"}
                           we will be asking  you a few questions to know better about you {"\n"}
                            lets move to the next steps !
                       
                       </Text>
                       </View>
                        <TouchableOpacity  style={{    padding: 5,
                         height: 100,
                         width: 100,  //The Width must be the same as the height
                         borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
                         borderColor:'silver',
                         borderWidth:1,
                         justifyContent:'center',
                         alignSelf:'center',
                         position:'absolute',
                         bottom:0,
                         marginBottom:10
                      
                         }} onPress={()=>this.props.navigation.navigate('Self_with_age')}>
                        <Text style={{color:'silver',fontSize:20,textAlign:'center',}}>NEXT</Text>
                        </TouchableOpacity>
                </View>
        </View>
    );
}

}

const AppNavigator = createStackNavigator({
    IntroWelcome:{
        screen:IntroWelcome,
        navigationOptions: {
          header: null // Will hide header for all screens of current stack 
            
          }
                },
                             
                Self_with_age:{
      screen:Self_with_age,
     navigationOptions: {
       header: null // Will hide header for all screens of current stack 
         
       }
  
          },  
                   
          });
export default createAppContainer(AppNavigator);    

const styles=StyleSheet.create({
container:{
    flex:1,
    
},
image:{
    flex:4.5,
    justifyContent:'center',
    alignItems:'center',

   
},
welcome:{
flex:3,

justifyContent:'flex-start',
alignItems:'center',

},

descriptions_with_button:{
    flex:8,
     justifyContent:'center',
     alignItems:'center',
        textAlign:'center',

    
}

})