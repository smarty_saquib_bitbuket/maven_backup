import React,{Component} from 'react'
import {View,Text,StyleSheet,Image,TouchableOpacity,SafeAreaView} from 'react-native'
import * as Animatable from "react-native-animatable";
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import FadeInView from 'react-native-fade-in-view';
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import { Ionicons,FontAwesome,MaterialCommunityIcons,MaterialIcons,AntDesign,Entypo} from '@expo/vector-icons'; // refer 
import { createStackNavigator } from 'react-navigation-stack';
import View_full_history from './View_full_history'
import Report from '../components/Report'

import { LinearGradient } from 'expo-linear-gradient';
class FadeInView1 extends React.Component{

    render(){
            return(

              
                <View style={styles.container}>
              <View style={styles.image}>
                        <View style={{flex:1,}}>
                        <Image

                        style={{ borderRadius: heightPercentageToDP(14.9) / 2,
                          height: heightPercentageToDP(14.9),
                          width: heightPercentageToDP(14.9),
                          borderWidth:3, //Then Make the Border Radius twice the size of width or Height   
                            borderColor:'#b2ddc1',
                           
                           
                         }}
                        source={require('../assets/doctor.jpeg')}
                        />
                        </View>
                     
                        <View style={{flex:1.75,}}>
                            <Text style={{fontSize:heightPercentageToDP(2.8),}}>Dr. Shawn</Text>
                            <Text style={{fontSize:heightPercentageToDP(2.6),color:'#B2DDC1'}}>Lactation Expert</Text>


                            </View>
                        </View>

                 <View style={styles.card_details}>
                 <LinearGradient
          colors={['#b2ddc1', 'transparent']}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            height: 200,
          }}
        />
                            <View style={{flex:1,justifyContent:'center'}}>
                            <Text style={{color:'white',fontSize:16}}>A 10 Minute Video Call</Text>
                            </View>
                            
                            <Animatable.View style={{flex:1,width:'100%',alignItems:'center',alignItems:'center',backgroundColor:'#f8f5f52e;'}} animation="slideInDown" iterationCount={0} direction="alternate">

                         <View style={{
                             justifyContent:'space-evenly',
                             alignItems:'center',
                               borderWidth:0.1,
                               borderColor:'#f7f7f7',
                             borderRadius:15,
                             backgroundColor:'#ece7e73b',
                       flex:1,
                       width:'75%',
                    
                      shadowColor: "#f8f5f52e;",
                      shadowOffset: {
                          width: 0,
                      
                      },
                    

                            }}>
                                                
                                                

                                                          <FadeInView
    duration={3500}
    style={{ }}
 
  >
                             <Text style={{fontSize:19,color:'white', padding:8}}>
                                 Friday, October 08
                                </Text>
                                <Text style={{fontSize:19,color:'white',padding:8}}>
                                08:30 pm - 08:40 pm
                                </Text></FadeInView>
                            </View>
                            </Animatable.View>


                           <View style={{flex:1,justifyContent:'center'}}>
                            
                            <Text style={{fontSize:35,color:'white',fontWeight:'bold'}}><FontAwesome name='rupee' size={30} color={'white'}/>3500</Text>
                            </View>
                            <View style={{flex:1,width:widthPercentageToDP(64.8),justifyContent:'center'}}>
                            <TouchableOpacity onPress={()=>this.props.navigation.navigate('View_full_history')} style={{padding:heightPercentageToDP(1.2),height:heightPercentageToDP(6.4),backgroundColor:'white',borderRadius:50,borderWidth:1,borderColor:'#f7f7f7'}}>
                        <Text style={{color:'balck',fontSize:15.5,textAlign:'center'}}>Pay Now</Text>
                    </TouchableOpacity>
                            </View>
                 </View>
                    
                </View>

            );

    }
}




const AppNavigator = createStackNavigator({
    FadeInView1:{
        screen:FadeInView1,
        navigationOptions: {
          header: null // Will hide header for all screens of current stack 
            
          }
                },
                             
                View_full_history:{
      screen:View_full_history,
     navigationOptions: {
       header: null // Will hide header for all screens of current stack 
         
       }
  
          },  
         
                   
          });
export default createAppContainer(AppNavigator);    


      
      
      

const styles=StyleSheet.create({
    container:{
        flex:1,
        padding:23,
        backgroundColor:'white'
      
       
      
        
    },
    image:{
        flex:1,
        flexDirection:'row',
        backgroundColor:'white',
        alignItems:'center',
      
    },
    card_details:{
        flex:2,
        backgroundColor:'#B2DDC1',
        justifyContent:'space-around',
        alignItems:'center',
        borderRadius:5,
        borderWidth:0.7,
        borderColor:'#c4c4c4',
        marginBottom:15 ,       
    }
})