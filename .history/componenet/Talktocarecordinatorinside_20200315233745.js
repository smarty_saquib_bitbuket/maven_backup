import React,{Component} from 'react'
import * as Font from 'expo-font';
import {View ,Text,StyleSheet,FlatList,Alert,Image,Modal, TouchableHighlight,button} from 'react-native'
import { Searchbar, IconButton } from 'react-native-paper';
import { Ionicons,FontAwesome,MaterialCommunityIcons,MaterialIcons,AntDesign,Entypo} from '@expo/vector-icons'; // refer 
import Triangle from 'react-native-triangle'; 
// https://github.com/Jpoliachik/react-native-triangle
import { createBottomTabNavigator } from 'react-navigation-tabs';
import styled from 'styled-components';
//https://styled-components.com/docs/basics
import {Card} from 'react-native-shadow-cards';
//https://github.com/Aamirali86/react-native-shadow-cards

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import FadeInView from 'react-native-fade-in-view';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import FadeInView1 from './FadeInView1'
import { TouchableOpacity } from 'react-native-gesture-handler';
class Talktocarecordinatorinside extends React.Component{

      call=(data,date)=>{
      this.setState({
      data:data,
      date:date
  })
    
  this.setModalVisible(!this.state.modalVisible);
        
  }
      state={
      data:'',
      date:'',
    }
  componentDidMount() {
    Font.loadAsync({
      'open-sans-Regular': require('../assets/fonts/SourceSerifPro-Regular.ttf'),
    });
  }
    
    renderSeparator = () => {  
        return (  
        <View  
          style={{  
          height: 1,  
          marginLeft:30,
          marginRight:30,
          backgroundColor:'#c4c4c4'

              
            }}  
        />  
        );  
    };  
    //handling onPress action  
    getListViewItem = (item) => {  
        Alert.alert(item.key);  
    }  




    
        state = {
          modalVisible: true,
          
        };
      
        setModalVisible(visible) {
          
          this.setState({modalVisible: visible});
        }
      

    render(){
 

        
        return(

          
     
            <View style={styles.container }>
                
        
                    <View style={{height:'20%',justifyContent:'center',alignItems:'center'}}>
          
             
               <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('please Select the date');
          }}>


            
<View style={styles.container1}>
              
<View style={styles.TriangleShapeCSS}/>

<View style={styles.card}>
  
<View style={{flex:1,flexDirection:'row',backgroundColor:'white',margin:0.9}}>

        <View style={{flex:1,flexDirection:'column',justifyContent:"space-evenly",alignItems:'center'}}>
        <View style={{}}>
          
        <FadeInView duration={2500} style={{ }}><Text style={{textAlign:'center',color:'#c4c4c4',fontSize:12,}}>FRI</Text></FadeInView></View>
        <TouchableHighlight
        onPress={() => {
        this.setModalVisible(!this.state.modalVisible);
        }}>
         <FadeInView duration={2500} style={{ }}><Text style={{textAlign:'center',fontSize:18,fontWeight:'bold',textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#c4c4c4' }} onPress={()=>this.call("Friday","04th")} >04</Text></FadeInView>
        </TouchableHighlight>
        </View>






        <View style={{flex:1,flexDirection:'column',justifyContent:"space-evenly",alignItems:'center'}}>
        <View style={{}}>
          
        <FadeInView duration={2500} style={{ }}><Text style={{textAlign:'center',color:'#c4c4c4',fontSize:12}}>SAT</Text></FadeInView></View>
        <TouchableHighlight
        onPress={() => {
        this.setModalVisible(!this.state.modalVisible);
        }}>
         <FadeInView duration={2500} style={{ }}><Text style={{textAlign:'center',fontSize:18,fontWeight:'bold',textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#c4c4c4' }} onPress={()=>this.call("Saturday","05th")} >05</Text></FadeInView>
        </TouchableHighlight>
        </View>


        <View style={{flex:1,flexDirection:'column',justifyContent:"space-evenly",alignItems:'center'}}>
        <View style={{}}>
          
        <FadeInView duration={2500} style={{ }}><Text style={{textAlign:'center',color:'#c4c4c4',fontSize:12}}>SUN</Text></FadeInView></View>
        <TouchableHighlight
        onPress={() => {
        this.setModalVisible(!this.state.modalVisible);
        }}>
         <FadeInView duration={2500} style={{ }}><Text style={{textAlign:'center',fontSize:18,fontWeight:'bold',textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#c4c4c4' }} onPress={()=>this.call("Sunday","06th")} >06</Text></FadeInView>
        </TouchableHighlight>
        </View>


        <View style={{flex:1,flexDirection:'column',justifyContent:"space-evenly",alignItems:'center'}}>
        <View style={{}}>
          
        <FadeInView duration={2500} style={{ }}><Text style={{textAlign:'center',color:'#c4c4c4',fontSize:12}}>MON</Text></FadeInView></View>
        <TouchableHighlight
        onPress={() => {
        this.setModalVisible(!this.state.modalVisible);
        }}>
         <FadeInView duration={2500} style={{ }}><Text style={{textAlign:'center',fontSize:18,fontWeight:'bold',textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#c4c4c4' }} onPress={()=>this.call("Monday","07th")} >07</Text></FadeInView>
        </TouchableHighlight>
        </View>


        <View style={{flex:1,flexDirection:'column',justifyContent:"space-evenly",alignItems:'center'}}>
        <View style={{}}>
          
        <FadeInView duration={2500} style={{ }}><Text style={{textAlign:'center',color:'#c4c4c4',fontSize:12}}>TUE</Text></FadeInView></View>
        <TouchableHighlight
        onPress={() => {
        this.setModalVisible(!this.state.modalVisible);
        }}>
         <FadeInView duration={2500} style={{ }}><Text style={{textAlign:'center',fontSize:18,fontWeight:'bold',textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#c4c4c4' }} onPress={()=>this.call("Tueday","08th")} >08</Text></FadeInView>
        </TouchableHighlight>
        </View>


        <View style={{flex:1,flexDirection:'column',justifyContent:"space-evenly",alignItems:'center'}}>
        <View style={{}}>
          
        <FadeInView duration={3000} style={{ }}><Text style={{textAlign:'center',color:'#c4c4c4',fontSize:12}}>WED</Text></FadeInView></View>
        <TouchableHighlight
        onPress={() => {
        this.setModalVisible(!this.state.modalVisible);
        }}>
         <FadeInView duration={3000} style={{ }}><Text style={{textAlign:'center',fontSize:18,fontWeight:'bold',textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#c4c4c4' }} onPress={()=>this.call("Wednesday","09th")} >09</Text></FadeInView>
        </TouchableHighlight>
        </View>


        <View style={{flex:1,flexDirection:'column',justifyContent:"space-evenly",alignItems:'center'}}>
        <View style={{}}>
          
        <FadeInView duration={3000} style={{ }}><Text style={{textAlign:'center',color:'#c4c4c4',fontSize:12}}>THU</Text></FadeInView></View>
        <TouchableHighlight
        onPress={() => {
        this.setModalVisible(!this.state.modalVisible);
        }}>
         <FadeInView duration={3000} style={{ }}><Text style={{textAlign:'center',fontSize:18,fontWeight:'bold',textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#c4c4c4' }} onPress={()=>this.call("Thursday","10th")} >10</Text></FadeInView>
        </TouchableHighlight>
        </View>


             </View>
             </View>
             </View>
             </Modal>




      <View style={{height:'50%', justifyContent:"center",alignSelf:"center",}}>   
      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
      <TouchableHighlight   style={styles.button}
      onPress={() => {
      this.setModalVisible(true);
      }}>
      <Text style={{textAlign:'center',fontSize:16.8,color:'#fff',}}>{this.state.data} on {this.state.date} October, 2009</Text>
      </TouchableHighlight> 
      </View>      
      </View>
      </View>


 

            
            <View style={{margin:22}}>
             <FlatList 

            data={[


            {key: '7.00 pm - 7.30 pm'},
            {key: '8.00 pm - 8.30 pm'},
            {key: '9.00 pm - 9.30 pm'},
            {key: '10.00 pm - 10.30 pm'},
            {key: '11.00 pm - 11.30 pm'},
            {key: '12.00 am - 12.30 am'},

            ]}

            
            renderItem={({item}) => 
            <TouchableOpacity
            onPress={()=>this.props.navigation.navigate('FadeInView1')}
            >
            <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
            <Text style={styles.item}>{item.key}
            </Text>
            <View style={{}}><AntDesign name='right' size={20} color={'#b2ddc1'}/></View>
            </View>
            </TouchableOpacity>
            }
            />
            </View>
 </View>

        );
    }

}



// class Community extends React.Component {
//     render() {
//       return (
//         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',backgroundColor:'white'}}>
//           <Text>Under working</Text>
//         </View>
//       );
//     }
//   }
 
//   class general extends React.Component {
//     render() {
//       return (
//         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//           <Text>general page</Text>
//         </View>
//       );
//     }
//   }
  



// const TabNavigator = createBottomTabNavigator({
  
  
//     Tab_with_list: {
        
//         screen:Tab_with_list,
//         navigationOptions:{
//             tabBarLabel:'Find Care',
//             tabBarIcon:({tintColor})=>(
//                 <FontAwesome name='heartbeat' color={tintColor} size={25}/>
              
//             )
            
//         }

//     },



//     Community: {
//         screen:Community,
//         navigationOptions:{
//             tabBarLabel:'Community',
          
//             tabBarIcon:({tintColor})=>(
//                 <MaterialCommunityIcons name='file-document' color={tintColor} size={25}/>
//             )
            
//         }
//     },



//     general: {
//         screen:general,
//         navigationOptions:{
//             tabBarLabel:'general',
//             tabBarIcon:({tintColor})=>(
//             <MaterialIcons name='person' color={tintColor} size={25}/>
//             )
            
//         }
//     },
   
//   },





//   {
//       initialRouteName:'Community',
    
//       tabBarOptions:{
//           activeTintColor:'#9eddcf',
//           inactiveTintColor:'gray',
//           style:{
            
//             borderTopWidth:0,
//             paddingBottom:5,
//             backgroundColor:'white'
//           },
          
          
//       }

//     } );



    
// export default createAppContainer(TabNavigator);
  
const AppNavigator = createStackNavigator({
  appoinmentfixdate:{
      screen:appoinmentfixdate,
      navigationOptions: {
        header: null // Will hide header for all screens of current stack 
          
        }
              },
                           
  FadeInView1:{
    screen:FadeInView1,
   navigationOptions: {
     header: null // Will hide header for all screens of current stack 
       
     }

        },  
                 
        });
export default createAppContainer(AppNavigator);    


const styles=StyleSheet.create({

  container1: {
    flex: 1,
    
    alignItems: 'center',
  
    paddingTop:heightPercentageToDP(10),
  
    backgroundColor:'#E9e9e9',
    opacity:0.68,
   
    
  },
 


  card:{

    backgroundColor:'#ffffff',
    height:"13.5%",
    width:'92.5%',
    borderRadius:5,
    borderColor:'white',
    borderWidth:0.9,
    borderColor:'#ffffff',
    zIndex:1,

shadowColor: "#ffff",
shadowOffset: {
	width: 0,
	height: 8,
},
  },
TriangleShapeCSS: {
 
  width: 0,
  height: 0,
  borderLeftWidth: 11,
  borderRightWidth: 11,
  borderBottomWidth: 13,
  borderStyle: 'solid',
  backgroundColor: 'transparent',
  borderLeftColor: 'transparent',
  borderRightColor: 'transparent',
  borderBottomColor: '#fff',
  borderTopColor:'#ffff',
  overflow:"hidden",
  position: 'relative',
  marginTop:22,
  zIndex:1
},
 
container: {
  flex: 1,
  height:'100%',
  backgroundColor:'#fff'
 },
 item: {
   margin:12,
   fontSize: 18,
   height: 35,
   color:'#727d85',
   
 },




 button:{
    height:heightPercentageToDP(6.2),
    width:widthPercentageToDP(77.5),
    borderWidth:1,
    borderColor:'#9eddcf',
    borderRadius:50,
    
    backgroundColor:'#9edccf',
    justifyContent:"center",
    shadowColor: "#c4c4c4",
    shadowOffset: {
        width: 0,
        height: 5,
    },
    shadowOpacity: 0.50,
    shadowRadius: 12,
    
    elevation: 2,
    
    }
})



