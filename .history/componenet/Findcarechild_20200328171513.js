import React,{Component} from 'react'
import * as Font from 'expo-font';
import {View ,Text,StyleSheet,FlatList,Alert,Image} from 'react-native'
import { Searchbar, IconButton } from 'react-native-paper';
import { Ionicons,FontAwesome,MaterialCommunityIcons,MaterialIcons,AntDesign,Entypo} from '@expo/vector-icons'; // refer 
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import { createBottomTabNavigator } from 'react-navigation-tabs';9

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Listofdoctor from './Listofdoctor'
import { TouchableOpacity } from 'react-native-gesture-handler';
class Findcarechild extends React.Component{

  componentDidMount() {
    Font.loadAsync({
      'open-sans-Regular': require('../assets/fonts/SourceSerifPro-Regular.ttf'),
    });
  }
  
    renderSeparator = () => {  
        return (  
            <View  
                style={{  
                    height: 1,  
                   marginLeft:30,
                   marginRight:30,
                   backgroundColor:'#c4c4c4'
                  
                  
                }}  
            />  
        );  
    };  
    //handling onPress action  
    getListViewItem = (item) => {  
        Alert.alert(item.key);  
    }  
    render(){
 
        return(

          
     
            <View style={styles.container }>
                <View style={{flexDirection:'column',margin:heightPercentageToDP(2.15)}}>
                    <Text style={{fontSize:heightPercentageToDP(3.1),alignSelf:'center',color:'#757171',fontFamily: 'open-sans-Regular',padding:10 }}>FIND CARE</Text>
               
                  <Searchbar 
                     style={{flexDirection:'row',}} placeholder="Search by condition or speciality"
                         />
                              </View>

 

 
         <View style={{margin:heightPercentageToDP(2.9),}}>
                          
         <FlatList 
        
         data={[
         {key: 'Ace'},
            
            {key: 'Allergies'},
            {key: 'Breathing techniques'},
            {key: 'Childrens nutrition'},
            {key: 'Chronic pain management'},
            {key: 'Digestive health'},
            {key: 'Eczema'},
            {key: 'Fungal infection'},
            {key: 'Heart health'},
            {key: 'Injusry recovery'},

            {key: 'Eczema'},
            {key: 'Fungal infection'},
            {key: 'Heart health'},
            {key: 'Injusry recovery'},

          ]}
          renderItem={({item}) => 
          <TouchableOpacity onPress={()=>this.props.navigation.navigate('Listofdoctor')}>
          <View style={{flexDirection:'row',justifyContent:'space-between'}}>
          <Text style={styles.item}>{item.key}</Text>
          <AntDesign name='right' size={24} color={'#b2ddc1'}/>
          </View>
          </TouchableOpacity>
        }
        />
      </View>
  
               
                                                   </View>

        );
    }

}



// class Community extends React.Component {
//     render() {
//       return (
//         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',backgroundColor:'white'}}>
//           <Text>Under working</Text>
//         </View>
//       );
//     }
//   }
 
  // class general extends React.Component {
  //   render() {
  //     return (
  //       <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
  //         <Text>general page</Text>
  //       </View>
  //     );
  //   }
  // }
  



// const TabNavigator = createBottomTabNavigator({
  
  
//     Tab_with_list: {
        
//         screen:Tab_with_list,
//         navigationOptions:{
//             tabBarLabel:'Find Care',
//             tabBarIcon:({tintColor})=>(
//                 <FontAwesome name='heartbeat' color={tintColor} size={25}/>
              
//             )
            
//         }

//     },



//     Community: {
//         screen:Community,
//         navigationOptions:{
//             tabBarLabel:'Community',
          
//             tabBarIcon:({tintColor})=>(
//                 <MaterialCommunityIcons name='file-document' color={tintColor} size={25}/>
//             )
            
//         }
//     },



//     general: {
//         screen:general,
//         navigationOptions:{
//             tabBarLabel:'general',
//             tabBarIcon:({tintColor})=>(
//             <MaterialIcons name='person' color={tintColor} size={25}/>
//             )
            
//         }
//     },
   
//   },





//   {
//       initialRouteName:'Community',
    
//       tabBarOptions:{
//           activeTintColor:'#9eddcf',
//           inactiveTintColor:'gray',
//           style:{
            
//             borderTopWidth:0,
//             paddingBottom:5,
//             backgroundColor:'white'
//           },
          
          
//       }

//     } );



    
// export default createAppContainer(TabNavigator);
  
  
const AppNavigator = createStackNavigator({
  Findcarechild:{
      screen:Findcarechild,
      navigationOptions: {
        header: null // Will hide header for all screens of current stack 
          
        }
              },
                           
              Listofdoctor:{
    screen:Listofdoctor,
   navigationOptions: {
     header: null // Will hide header for all screens of current stack 
       
     }

        },  
                 
        });
export default createAppContainer(AppNavigator);    

const styles=StyleSheet.create({


container: {
  flex: 1,
  paddingTop: 20,
  backgroundColor:'white'
 },
 item: {
  
   fontSize: 17,
   height: 44,
   color:'#727d89'
 },

})



