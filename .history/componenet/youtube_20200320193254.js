import React, { Component } from 'react';
import { Text, View,WebView} from 'react-native';

export default class HelloWorldApp extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
          <WebView  
    source = {{ uri:'https://www.javatpoint.com' }}  
/>  
      </View>
    );
  }
}
