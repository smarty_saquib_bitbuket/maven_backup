import React,{Component} from 'react'
import {View ,Text,StyleSheet,FlatList,Alert,Image,TouchableOpacity} from 'react-native'
import { Searchbar, IconButton } from 'react-native-paper';
import { Ionicons,FontAwesome,MaterialCommunityIcons,MaterialIcons,Entypo} from '@expo/vector-icons'; // refer 
import * as Font from 'expo-font';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';
export default class Viewpasthistory extends React.Component{


  componentDidMount() {
    Font.loadAsync({
      'open-sans-Regular': require('../assets/fonts/SourceSerifPro-Regular.ttf'),
    });
    
  }


    render(){

        return(
            <View style={{ backgroundColor:'white', padding:10,paddingTop:0,}}>
              <ScrollView>
            <View>

            <Text style={{textAlign:'center',fontSize:20,margin:22,color:'#9eddcf',textDecorationLine: 'underline',backgroundColor:'white',fontFamily:'open-sans-Regular' }} >View Past Appoinments</Text>
                      

            </View>
            <FlatList  
            data={[  {titledDate:' DATE',titledtday:' Today',subtitleAT:' AT',subtitleTime:'  3.30 PM ',avtar:require('../assets/nigeropatient.jpg'),
            },
            {titledDate:' DATE',titledtday:' Today',subtitleAT:' AT',subtitleTime:'  3.30 PM ',avtar:require('../assets/ritikaamru.jpeg'),
          },
          {titledDate:' DATE',titledtday:' Today',subtitleAT:' AT',subtitleTime:'  3.30 PM ',avtar:require('../assets/ritikaamru.jpeg'),
        },
        {titledDate:' DATE',titledtday:' Today',subtitleAT:' AT',subtitleTime:'  3.30 PM ',avtar:require('../assets/ritikaamru.jpeg'),
      },
      {titledDate:' DATE',titledtday:' Today',subtitleAT:' AT',subtitleTime:'  3.30 PM ',avtar:require('../assets/ritikaamru.jpeg'),
    },
    {titledDate:' DATE',titledtday:' Today',subtitleAT:' AT',subtitleTime:'  3.30 PM ',avtar:require('../assets/ritikaamru.jpeg'),
  },
  {titledDate:' DATE',titledtday:' Today',subtitleAT:' AT',subtitleTime:'  3.30 PM ',avtar:require('../assets/ritikaamru.jpeg'),
},

{titledDate:' DATE',titledtday:' Today',subtitleAT:' AT',subtitleTime:'  3.30 PM ',avtar:require('../assets/ritikaamru.jpeg'),
},



            ]}  
            renderItem={({item}) =>  

         
                        <View style={styles.card}>

                          
                        <View style={{flexDirection:'row',margin:heightPercentageToDP(3.1),}}>
                       
                        <View style={{flex:1.2,}}>
                       <View style={{flex:1,flexDirection:'row',alignItems:'center'}}><Text style={{fontSize:12.5,color:'#c4c4c4',}}>{item.titledDate}</Text>
                        <Text style={{fontSize:15.5,paddingLeft:10, textShadowOffset: {width: 2,height: 2},textShadowRadius: 10,textShadowColor:'#c4c4c4'}}>{item.titledtday}</Text></View>
                       
                        <View style={{flex:1,flexDirection:'row',alignItems:'center'}}><Text style={{fontSize:12.5,color:'#c4c4c4',paddingLeft:7}}>{item.subtitleAT}</Text>
                        <Text style={{fontSize:15.5,paddingLeft:15,textShadowRadius: 10,textShadowColor:'#c4c4c4'}}>{item.subtitleTime}</Text></View>
                        
                        <Text style={{color:'silver',fontSize:14}}>{item.experience}</Text>
                        <View>
                        <View>
                          <View style={{flex:1,flexDirection:'column'}}>
                    
                        <Text style={{color:'silver',fontSize:14}}>{item.experience}</Text>
                        </View>
                        </View>
                        </View>
                        </View>
                        
                       
                        <View style={{flexDirection:'row',flex:1,justifyContent:'space-around',}}>
                            <View
                                style={{
                                borderLeftWidth: 1,
                                borderLeftColor: '#dddddd80',
                                }}
                                />
<View>
                        <Image
                        style={{borderRadius: heightPercentageToDP(8.1) / 2,
                          height: heightPercentageToDP(8.1),
                          width: heightPercentageToDP(8.1),borderWidth:3, borderColor:'#A1EF8B'}}
                        source={item.avtar}
                        />
                        
                        <Text style={{fontSize:11, textAlign:'center' ,marginTop:heightPercentageToDP(1),textShadowRadius: 10,textShadowColor:'#c4c4c4'}}>RITIKA AMRU</Text>
                        </View>
                        
             
                        </View>
                        </View>
                        
                      
                       
                       </View>
                      
                      }  
                    ItemSeparatorComponent={this.renderSeparator}  
           
        
            />  
            </ScrollView>
               
             </View>

        );
    }

}

// class Community extends React.Component {
//     render() {
//       return (
//         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//           <Text>Under working</Text>
//         </View>
//       );
//     }
//   }

//   class general extends React.Component {
//     render() {
//       return (



   
//         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//           <Text>general page</Text>
//         </View>
//       );
//     }
//   }



// const TabNavigator = createBottomTabNavigator({
//     E: {
        
//         screen:E,
//         navigationOptions:{
//             tabBarLabel:'Find Care',
//             tabBarIcon:({tintColor})=>(
//                 <FontAwesome name='heartbeat' color={tintColor} size={24}/>
//             )
            
//         }

//     },
//     Community: {
//         screen:Community,
//         navigationOptions:{
//             tabBarLabel:'Community',
//             tabBarIcon:({tintColor})=>(
//                 <MaterialCommunityIcons name='file-document' color={tintColor} size={24}/>
//             )
            
//         }
//     },
//     general: {
//         screen:general,
//         navigationOptions:{
//             tabBarLabel:'me',
//             tabBarIcon:({tintColor})=>(
//                 <MaterialIcons name='person' color={tintColor} size={24}/>
//             )
            
//         }
//     },
   
//   },
//   {
//       initialRouteName:'Community',
    
//       tabBarOptions:{
//           activeTintColor:'#B2ddc1',
//           inactiveTintColor:'gray',
//           style: {
//             backgroundColor: 'white',//color you want to change
//             borderTopWidth:0
//           }

//       }

//     } );
  
// export default createAppContainer(TabNavigator);
  
  

const styles=StyleSheet.create({
container:{
   flex:1,
backgroundColor:'white'
   
   
},
card:{
    
    flex:2,
    borderWidth:1,
    borderColor:'#f7f7f7',
    margin: 10,
    borderRadius:5,
  
shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 4,
},
shadowOpacity: 0.32,
shadowRadius: 5.46,
backgroundColor:'white',
height:heightPercentageToDP(17.4),
elevation: 9,
borderRadius:20,
backgroundColor:'white'

}

})