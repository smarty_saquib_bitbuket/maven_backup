import React,{Component} from 'react'
import {View,Text,StyleSheet,Image,TouchableOpacity} from 'react-native'
import { createBottomTabNavigator } from 'react-navigation-tabs';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import appoinmentfixdate from './appoinmentfixdate'
import { Ionicons,FontAwesome,MaterialCommunityIcons,MaterialIcons,AntDesign,Entypo} from '@expo/vector-icons'; // refer 



class Profile_with_details extends React.Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.subcontainer}>
                    <View style={styles.image}>
                        <View style={{flex:1,justifyContent:'center',}}>
                        <Image style={{borderRadius: heightPercentageToDP(16.7) / 2,
height: heightPercentageToDP(16.7),
width: heightPercentageToDP(16.7),borderWidth:2.5,borderColor:'#9eddcf'}}
                        source={require('../assets/doctor.jpeg')}
                        />
                        </View>
                    </View>
             <View style={styles.text_with_button}>
                 <View style={{flex:2,justifyContent:"flex-end"}}>
                     
                         <Text style={{color:'#000000',fontSize:heightPercentageToDP(2.67),padding:2}}> Erika Mars</Text>
                          <Text style={{color:'#9eddcf',fontSize:heightPercentageToDP(2.45),padding:2}}> Lactation Expert</Text>

                            <TouchableOpacity style={{backgroundColor:'#9eddcf',borderRadius:20,marginTop:10,padding:4,width:widthPercentageToDP(29),height:heightPercentageToDP(3.7)}}>
                        <Text style={{color:'white',fontSize:heightPercentageToDP(1.66),textAlign:'center'}}>Watch Video</Text>
                    </TouchableOpacity>
                        </View>

                    </View>
                  
                </View>
                <View style={{flex:3.5, margin:25,}}>
                    <View style={{flex:1,justifyContent:'space-around'}}>
                        <Text style={{color:'#9eddcf', fontSize:heightPercentageToDP(2.3),paddingBottom:heightPercentageToDP(2)}}>Certification:</Text>
                           <Text style={{textAlign:"left",color:'#c4c4c4',fontSize:14,}}>
                              It is a long established fact that a reader will be distracted by the readable content of a page when looking it's layout
                                </Text>
                                </View>
                             <View style={{flex:1,justifyContent:'space-around',paddingTop:20}}>
                        <Text  style={{color:'#9eddcf',fontSize:heightPercentageToDP(2.3),paddingBottom:heightPercentageToDP(2)}}>Education:</Text>
                 <Text style={{textAlign:'left',color:'#c4c4c4',fontSize:14}}>  
             It is a long established fact that a reader will be distracted by the readable content of a page when looking it's layout
         </Text>
             </View>
             <View style={{flex:1,justifyContent:'space-around',}}>
                        <Text  style={{color:'#9eddcf',fontSize:heightPercentageToDP(2.3),marginTop:20}}>Years of experience:</Text>
                 <Text style={{textAlign:'left',color:'#c4c4c4',fontSize:14,paddingBottom:10}}>12
         </Text>
             </View>
                        <View style={{flex:1,justifyContent:'space-around'}}>
                        <Text  style={{color:'#9eddcf',fontSize:heightPercentageToDP(2.3),paddingBottom:10}}>Sub-specialities:</Text>
                        <Text style={{color:'#c4c4c4',fontSize:14}}>It is a long established fact that a reader will be distracted by the readable content of a page when looking it's layout
                            </Text>
                        </View>
                        <View style={{flex:1,justifyContent:'space-around'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('appoinmentfixdate')} style={{alignSelf:'center',width:widthPercentageToDP(57),padding:7, height:heightPercentageToDP(6.5),borderWidth:1,borderColor:'#f7f7f7',backgroundColor:'#90d6a9',borderRadius:30,marginTop:50}}>
                        <Text style={{color:'white',fontSize:heightPercentageToDP(2.6),textAlign:'center',}}>Book Doctor</Text>
                        </TouchableOpacity>
                        </View>
                </View>
                
            </View>
        );
    }
}



// class Community extends React.Component {
//     render() {
//       return (
//         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//           <Text>Under working</Text>
//         </View>
//       );
//     }
//   }

//   class general extends React.Component {
//     render() {
//       return (



   
//         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//           <Text>general page</Text>
//         </View>
//       );
//     }
//   }



// const TabNavigator = createBottomTabNavigator({
//     E: {
        
//         screen:Profile_with_details ,
//         navigationOptions:{
//             tabBarLabel:'Find Care',
//             tabBarIcon:({tintColor})=>(
//                 <FontAwesome name='heartbeat' color={tintColor} size={24}/>
//             )
            
//         }

//     },
//     Community: {
//         screen:Community,
//         navigationOptions:{
//             tabBarLabel:'Community',
//             tabBarIcon:({tintColor})=>(
//                 <MaterialCommunityIcons name='file-document' color={tintColor} size={24}/>
//             )
            
//         }
//     },
//     general: {
//         screen:general,
//         navigationOptions:{
//             tabBarLabel:'me',
//             tabBarIcon:({tintColor})=>(
//                 <MaterialIcons name='person' color={tintColor} size={24}/>
//             )
            
//         }
//     },
   
//   },
//   {
//       initialRouteName:'Community',
    
//       tabBarOptions:{
//           activeTintColor:'#B2ddc1',
//           inactiveTintColor:'gray',
//           style: {
//             backgroundColor: 'white',//color you want to change
//             borderTopWidth:0
//           }

//       }

//     } );
  
// export default createAppContainer(TabNavigator);
const AppNavigator = createStackNavigator({
    Profile_with_details:{
        screen:Profile_with_details,
        navigationOptions: {
          header: null // Will hide header for all screens of current stack 
            
          }
                },
                             
    appoinmentfixdate:{
      screen:appoinmentfixdate,
     navigationOptions: {
       header: null // Will hide header for all screens of current stack 
         
       }
  
          },  
                   
          });
  export default createAppContainer(AppNavigator);    
  
const styles=StyleSheet.create({
container:{
    backgroundColor:'white',
    flex:1,
},
subcontainer:{
    flex:1,
    flexDirection:'row',
    justifyContent:'space-evenly' ,
    margin:20,
    paddingTop:25,
  
    
},
image:{
    flex:3,
    
    flexDirection:'row',
    justifyContent:'center'
},
text_with_button:{
    flex:4.5,
    
}

})