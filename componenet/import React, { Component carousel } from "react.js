import React, { Component } from "react";
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
import { Dimensions, StyleSheet,View,Text,Image,TouchableOpacity,FlatList } from 'react-native';

const { width: screenWidth } = Dimensions.get('window')

export default class Appointment_for_today extends Component {
  constructor() {
    super();

    this.state = {
      entries: [
        {
          "ID": "001", 
          "Name": "001", 
          "thumbnail": "https://wallpapercave.com/wp/KaNO7Ya.jpg"
      },

      {
          "ID": "002", 
          "Name": "002", 
          "thumbnail": "https://wallpapercave.com/wp/ZxV8qRo.jpg"
      },

      {
          "ID": "003", 
          "Name": "003", 
          "thumbnail": "https://wallpapercave.com/wp/ytM5xOy.jpg"
      }
      ,

      {
          "ID": "004", 
          "Name": "004", 
          "thumbnail": "https://wallpapercave.com/wp/STgHPst.jpg"
      }
      ,

      {
          "ID": "005", 
          "Name": "005", 
          "thumbnail": "https://wallpapercave.com/wp/vg5q7pY.jpg"
      }
      ,

      {
          "ID": "006", 
          "Name": "006", 
          "thumbnail": "https://wallpapercave.com/wp/b2HsGgL.jpg"
      }
      ],
  }
}
    _renderItem ({item, index}, parallaxProps) {
        console.log(item);    
        return (


        
    <View style={styles.maincontainer}> 
        
              <View style={styles.container}> 
                  <View style={styles.item}>
                     <ParallaxImage
                        containerStyle={styles.imageContainer}
                           style={styles.image}
                             parallaxFactor={0.4}
                               {...parallaxProps}
                                 showSpinner={true}
                                   spinnerColor={'cyan'}
                                         />
                                      <View style={styles.title} >
                                      <FlatList  
            data={[  {title:'DATE : TODAY',subtitle:' AT :3.30PM ',avtar:require('../assets/ritikaamru.jpeg'),
            },
           
         
       
     


            ]}  
            renderItem={({item}) =>  

         
                        <View style={styles.card}>

                          
                        <View style={{flexDirection:'row',margin:28,}}>
                       
                        <View style={{flex:1,}}>
                        <Text style={{fontSize:16,fontWeight:'bold'}}>{item.title}</Text>
                        <TouchableOpacity
         style={{height:35,
width:100,
padding:6,
borderWidth:1,
borderColor:'#fff',
borderRadius:50,
backgroundColor:'#b2ddc1',
marginTop:15,
}}
         onPress={()=>this.props.navigation.navigate('F')}
        
       >
         <Text style={{textAlign:"center",fontWeight:"strong",fontSize:13,color:'white'



         }} >in 2 hours</Text>
         </TouchableOpacity>
                        
                        <Text style={{color:'silver',fontSize:14}}>{item.experience}</Text>
                        <View>
                        <View>
                          <View style={{flex:1,flexDirection:'column'}}>
                    
                        <Text style={{color:'silver',fontSize:14}}>{item.experience}</Text>
                        </View>
                        </View>
                        </View>
                        </View>
                        
                       
                        <View style={{flexDirection:'row',flex:1,justifyContent:'space-between',}}>
                            <View
                                style={{
                                borderLeftWidth: 1,
                                margin:8,
                                borderLeftColor: '#dddddd80',
                                }}
                                />
<View>
                        <Image
                        style={{width:65,height:65,borderRadius:100,borderWidth:3, borderColor:'#A1EF8B'}}
                        source={item.avtar}
                        />
                        
                        <Text style={{fontSize:13, textAlign:'center' ,marginTop:8,}}>RITIKA AMRU</Text>
                        </View>
                        
             
                        </View>
                        </View>
                        
                      
                       
                       </View>
                      
                      }  
                  
        
            />  
                </View>
            </View>



                
            </View>


            
            </View>
            
        );
    }

    render () {
        return (
          <View style={styles.container}>
              <View style={{ justifyContent:"space-around", flex:1}}><Text style={{marginTop:12,fontSize:15.5,color:'white'}}>Appoinment For Today</Text></View>
            <Carousel
                sliderWidth={screenWidth}
                sliderHeight={screenWidth}
                itemWidth={screenWidth - 60}
                data={this.state.entries}
                renderItem={this._renderItem}
                hasParallaxImages={true}
            />
          </View>
          

        );
    }
}

const styles = StyleSheet.create({


  maincontainer:{
  
   
    alignItems:'center',

  }, 
  container:{
    flex:0.45,
    justifyContent:'space-between',
    alignItems:'center',

    backgroundColor:'#b2ddc1'
  },  
  item: {
    width: screenWidth - 60,
    height: screenWidth - 60,
    
  },

  title: {


    ...StyleSheet.absoluteFillObject,
        resizeMode: 'cover',
  },
  

  
   
card:{
   marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue

  
    borderWidth:1,
    borderColor:'#f7f7f7',
    borderRadius:5,
  

    backgroundColor:'orange',
    shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 4,
},
shadowOpacity: 0.32,
shadowRadius: 5.46,
backgroundColor:'white',
height:145,
elevation: 3,
borderRadius:20

},


})


// https://github.com/oblador/react-native-parallax
// https://stackoverflow.com/questions/57593529/react-native-dynamic-image-slider-transition-

