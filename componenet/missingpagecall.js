import React,{Component} from 'react'
import {View,Text,StyleSheet,TouchableOpacity,Image, Alert} from 'react-native'
import { Ionicons,FontAwesome,MaterialCommunityIcons} from '@expo/vector-icons'; // refer 

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import calling from '../componenet/calling'
class missingpagecall extends React.Component{
render(){
    return(
            
            <View style={styles.container}>
                
                <View style={styles.image}>
                        <View  style={{}}>
                        <Image
                      
                         style={{ height: 110,
                           
                            width: 110,  //The Width must be the same as the height
                            borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
                            borderColor:'silver',
                            borderWidth:1,
                            alignSelf:"center"
                            
                         }}
                        source={require('../assets/fruite.jpeg')}
                        />
                        </View>
                     
                            <View style={{marginLeft:'5%'}}>
                            <Text style={{fontSize:20,}}>Erika Mars</Text>
                            <Text style={{fontSize:16,}}>26 years old</Text>    
                            </View>
                            </View>

                            <View style={styles.box}>
                            <View style={{flexDirection:'row',}}>
                            <View style={{flex:2,alignItems:'center'}}> 
                            <Text style={{fontSize:18,color:'silver'}}>Date</Text>
                            </View>
                            <View style={{flex:3,alignItems:'center'}}> 
                            <Text style={{fontSize:20,}}>Thursday,</Text>
                            </View>
                            <View style={{flex:5,}}> 
                            <Text style={{fontSize:20}}>24th Nov</Text>
                            </View>

</View>
<View style={{flexDirection:'row'}}>
<View style={{flex:1,alignItems:'center'}}>
<Text style={{fontSize:18,color:'silver'}}>Time</Text>
</View>
<View style={{flex:4}}>
<Text style={{fontSize:20}}>3: 30 pm</Text>
</View>
</View>

<View style={{}}>
<TouchableOpacity onPress={() => Alert.alert("You Clicked on Button")} style={{padding:5,borderWidth:2,borderColor:'#90d6a9',backgroundColor:'#90d6a9',borderRadius:30 ,justifyContent:'center',width:'80%',alignSelf:'center'}}>

<View style={{flexDirection:'row',}}>


<Text style={{flex:2.5,textAlign:'center',fontSize:18}}>2 Days</Text>
<View
style={{
borderLeftWidth: 1,
borderLeftColor: 'white',
}}
/>
                             <Text style={{flex:3,fontSize:18,textAlign:'center'}}>2 Hours</Text>
                             <View
                                style={{
                                borderLeftWidth: 1,
                                borderLeftColor: 'white',
                                }}
                                />
                             <Text style={{flex:3,fontSize:18,textAlign:'center'}}>38 mins</Text>
                             
                             </View>
                             </TouchableOpacity>

                            </View>
                          
                        </View>
                        <View style={styles.paragraph}>
                        <Text style={{fontSize:16,color:'silver'}}>It is a long established fact that a reader
                        will be distracted by the readable
                        content of a page when looking at its
                        layout. The point of using Lorem Ipsum
                        is that it has a more-or-less normal
                        distribution of letters, as opposed to
                        using 'Content here, content here',
                        making it look like readable English.</Text>
                        </View>
                        <View style={styles.View_full_history}>
                                <Text style={{fontSize:18,color:'#B2DDC1',alignSelf:'center'}}>View Full History</Text>
                        </View>
                        <View style={styles.button}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('calling')} style={{    padding: 5,
                         height: 100,
                         width: 100,  //The Width must be the same as the height
                         borderRadius:400, //Then Make the Border Radius twice the size of width or Height   
                         borderColor:'silver',
                         borderWidth:1,
                         justifyContent:'center',
                         alignSelf:'center',
                         backgroundColor:'silver'
                      
                         }}>
                             <View style={{justifyContent:'center',alignItems:'center'}}>
                        <Ionicons name='md-call' size={50} color={'white'}/>
                        </View>
                        </TouchableOpacity>
                        </View>
            </View>

         );

        }

    }

    const AppNavigator = createStackNavigator({
        missingpagecall:{
            screen:missingpagecall,
            navigationOptions: {
              header: null // Will hide header for all screens of current stack 
                
              }
                    },
                                 
                    calling:{
          screen:calling,
         navigationOptions: {
           header: null // Will hide header for all screens of current stack 
             
           }
      
              },  
                       
              });
    export default createAppContainer(AppNavigator);    
    
     
const styles=StyleSheet.create({
    container:{
        flex:1,
        padding:20,
        paddingBottom:0,
        
        backgroundColor:'white',
        
    },
    image:{
    flex:2.5,
    flexDirection:'row',
    
    alignItems:'center', 
},

box:{
    flex:2,
    borderWidth:1,
    borderColor:'black',
    borderRadius:5,
    justifyContent:'space-around',
},
paragraph:{
    flex:2.8,
    justifyContent:'center'
},
View_full_history:{
    flex:0.8,
  
},
button:{
    flex:2,

}
})