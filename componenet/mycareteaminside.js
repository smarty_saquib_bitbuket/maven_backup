// import React, { Component } from 'react';
// import { Text, View, Dimensions, StyleSheet } from 'react-native';

// import Carousel, { Pagination } from 'react-native-snap-carousel'; // Version can be specified in package.json


// const SLIDER_WIDTH = Dimensions.get('window').width;
// const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7);
// const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 3 / 4);

// const DATA = [];
// for (let i = 0; i < 10; i++) {
//   DATA.push(i)
// }

// export default class HelloWorldApp extends Component {
  
//   state = {
//     index: 0
//   }

//   constructor(props) {
//     super(props);
//     this._renderItem = this._renderItem.bind(this)
//   }

//   _renderItem({ item }) {
//     return (
//       <View style={styles.itemContainer}>
//         <Text style={styles.itemLabel}>{`Item ${item}`}</Text>
//       </View>
//     );
//   }
  

//   get pagination () {
//     const { entries, activeSlide } = this.state;
//     return (
//         <Pagination
//           dotsLength={DATA.length}
//           activeDotIndex={this.state.index}
//           containerStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.5)' }}
//           dotStyle={{
//               width: 10,
//               height: 10,
//               borderRadius: 5,
//               backgroundColor: 'rgba(255, 255, 255, 0.92)'
//           }}
//           inactiveDotStyle={{
//             backgroundColor:'white'
//               // Define styles for inactive dots here
//           }}
//           inactiveDotOpacity={0.4}
//           inactiveDotScale={0.6}
          
//         />
//     );
// }
//   render() {
//     return (
//       <View>
//         <Carousel
        
//         // autoplay={true}
//         // loop={true}
//           ref={(c) => this.carousel = c}
//           data={DATA}
//           renderItem={this._renderItem}
//           sliderWidth={SLIDER_WIDTH}
//           itemWidth={ITEM_WIDTH}
//           containerCustomStyle={styles.carouselContainer}
//           inactiveSlideShift={0}
//           onSnapToItem={(index) => this.setState({ index })}
//           useScrollView={true}  
             
//         />
//         <Text style={styles.counter}>
//           {this.state.index}
          

//         </Text>
//         { this.pagination }
      
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   carouselContainer: {
//     marginTop: 50
//   },
//   itemContainer: {
//     width: ITEM_WIDTH,
//     height: ITEM_HEIGHT,
//     alignItems: 'center',
//     justifyContent: 'center',
//     backgroundColor: 'dodgerblue'
//   },
//   itemLabel: {
//     color: 'white',
//     fontSize: 24
//   },
//   counter: {
//     marginTop: 25,
//     fontSize: 30,
//     fontWeight: 'bold',
//     textAlign: 'center'
//   }
// });


import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
const DATA = [];
for (let i = 0; i < 10; i++) {
  DATA.push(i)
}

const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 3 / 4);

const SCREEN_WIDTH = Dimensions.get('window').width

const Screen = (props) => (
  <View style={{ flex : 1 }}>
    <Text>{ props.text }</Text>
  </View>
)

export default class HelloWorldApp extends Component {
  
  SCREENS = [
    <Screen text='first screen' />,
    <Screen text='second screen' />,
    <Screen text='third screen' />
  ]
  

  _renderItem({ item }) {
    return (
      <View style={styles.itemContainer}>
        <Text style={styles.itemLabel}>{`Item ${item}`}</Text>
      </View>
    );
  }
  constructor(props) {
    super(props)
   this._renderItem = this._renderItem.bind(this)

    this.state = {
      activeTab : 0
    }
  }
  
  render() {
    return (
      <View style={styles.container}>
      
      <Carousel
        
                // autoplay={true}
                // loop={true}
                ref={ ref => this.carouselRef = ref }
                data={DATA}
                  renderItem={this._renderItem}
                  sliderWidth={SLIDER_WIDTH}
                  itemWidth={ITEM_WIDTH}
                  containerCustomStyle={styles.carouselContainer}
                  inactiveSlideShift={0}
                  onSnapToItem={(index) => this.setState({ index })}
                  useScrollView={true}  
                  onSnapToItem={ i => this.setState({ activeTab : i }) }

                />
        
        <View
          style={ styles.tabBar }
        >
          <Pagination
            containerStyle={ styles.tabsContainer }
            renderDots={ activeIndex => (
              DATA.map((screen, i) => (
                <TouchableOpacity
                  style={{flex : 1, alignItems : 'center' }}
                  key={ i }
                  onPress={() => {
                    this.carouselRef._snapToItem(this.carouselRef._getPositionIndex(i));
                  }}
                >
                 <Text
                   style={{ color : activeIndex === i ? 'blue' : '#000'  }}
                  >
                    { i }
                  </Text>
                </TouchableOpacity>
              ))
            )}
            activeDotIndex={ this.state.activeTab }
            dotsLength={DATA.length}
          />
        </View>
        
        
      </View>
    )
  }
}

const styles = StyleSheet.create({
 
   carouselContainer: {
    marginTop: 50
  },
  itemContainer: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'dodgerblue'
  },
  itemLabel: {
    color: 'white',
    fontSize: 24
  },
  counter: {
    marginTop: 25,
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center'
  }
})
