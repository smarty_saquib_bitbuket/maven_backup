
import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Dimensions, TouchableOpacity, SafeAreaView, Animated, Easing,AppRegistry, } from "react-native";
import * as FadeInView from "react-native-animatable";
import * as Animatable from "react-native-animatable";
export default class FadeInViews extends Component {

state={
Animated:new Animated.Value(0),

}
componentDidMount(){
  const {animated} = this.state;

  Animated.timing(animated,{
    toValue:1,
    duration:1000,
  })
}

  render() {


    return (
      <View style={styles.container}>
       
        
          <Animatable.View style={styles.card} animation="slideInUp" iterationCount={200} direction="alternate">
            
          </Animatable.View>
        
        
      </View>
    );
  }
}

const styles=StyleSheet.create({
 container:{

  flex:1,
  flexDirection:'row',

  justifyContent:'center',
  alignItems:'center',
  backgroundColor:'green'
 },
 card:{
  width:100,
  height:100,
  borderColor:'red',
  borderRadius:50,
  backgroundColor:'orange',
  transform:[

    {

      scale:Animated
    }
  ]

  
 }
 
})


