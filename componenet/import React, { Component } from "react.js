import React, { Component } from "react";
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
import { Dimensions, StyleSheet,View,Text } from 'react-native';

const { width: screenWidth } = Dimensions.get('window')

export default class HomeScreen extends Component {
  constructor() {
    super();

    this.state = {
      entries: [
        {
          "ID": "001", 
          "Name": "001", 
          "thumbnail": "https://wallpapercave.com/wp/KaNO7Ya.jpg"
      },

      {
          "ID": "002", 
          "Name": "002", 
          "thumbnail": "https://wallpapercave.com/wp/ZxV8qRo.jpg"
      },

      {
          "ID": "003", 
          "Name": "003", 
          "thumbnail": "https://wallpapercave.com/wp/ytM5xOy.jpg"
      }
      ,

      {
          "ID": "004", 
          "Name": "004", 
          "thumbnail": "https://wallpapercave.com/wp/STgHPst.jpg"
      }
      ,

      {
          "ID": "005", 
          "Name": "005", 
          "thumbnail": "https://wallpapercave.com/wp/vg5q7pY.jpg"
      }
      ,

      {
          "ID": "006", 
          "Name": "006", 
          "thumbnail": "https://wallpapercave.com/wp/b2HsGgL.jpg"
      }
      ],
  }
}
    _renderItem ({item, index}, parallaxProps) {
        console.log(item);
        
        
        
        
        
        return (

<View style={styles.container}>

            <View style={styles.item}>
                <View  style={{flex: 0.5,
    marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
    backgroundColor: 'white',
    borderRadius: 15,
    borderWidth:1,
    backgroundColor:'red'
   }}>







                <Text  numberOfLines={2}>
                    { item.Name }






                </Text>


      



                </View>
            </View>
            </View>
            







            
        );
    }

    render () {
        return (
          <View style={styles.container}>
            <Carousel
                sliderWidth={screenWidth}
                sliderHeight={screenWidth}
                itemWidth={screenWidth - 60}
                data={this.state.entries}
                renderItem={this._renderItem}
                hasParallaxImages={true}
            />
          </View>

        );
    }
}

const styles = StyleSheet.create({
  container:{

paddingTop:30,
flex:0.34,
justifyContent:'space-between',
    backgroundColor:'red'
  },  
  item: {
    width: screenWidth - 60,
    height: screenWidth - 60,
  },
  imageContainer: {
    flex: 0.5,
    marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
    backgroundColor: 'white',
    borderRadius: 15,
    borderWidth:5,
   
   
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    resizeMode: 'contain',
  },
  card:{
    
    flex:2,
    borderWidth:1,
    borderColor:'#f7f7f7',
    margin: 10,
    borderRadius:5,
  
    
    shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 4,
},
shadowOpacity: 0.32,
shadowRadius: 5.46,
backgroundColor:'red',
height:140,
elevation: 9,
borderRadius:20

}
})