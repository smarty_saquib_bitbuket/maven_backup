  import React, { Component } from "react";
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
import { Dimensions, StyleSheet,View,Text,Image,TouchableOpacity } from 'react-native';
import {widthPercentageToDP , heightPercentageToDP  } from 'react-native-responsive-screen'
import BpkCarouselIndicator from 'react-native-bpk-component-carousel-indicator';
const { width: screenWidth } = Dimensions.get('window')

export default class Appointment_for_today extends Component {
  constructor() {
    super();

    this.state = {
      entries: [
        {
          "ID": "001", 
          "Name": "001", 
          "thumbnail": "https://wallpapercave.com/wp/KaNO7Ya.jpg"
      },

      {
          "ID": "002", 
          "Name": "002",
          "ID": "003", 
          "Name": "003", 
          "thumbnail": "https://wallpapercave.com/wp/ytM5xOy.jpg"
      }
      ,

      {
          "ID": "004", 
          "Name": "004", 
          "thumbnail": "https://wallpapercave.com/wp/STgHPst.jpg"
      }
      ,

      {
          "ID": "005", 
          "Name": "005", 
          "thumbnail": "https://wallpapercave.com/wp/vg5q7pY.jpg"
      }
      ,

      {
          "ID": "006", 
          "Name": "006", 
          "thumbnail": "https://wallpapercave.com/wp/b2HsGgL.jpg"
      }
      ],
  }
}
    _renderItem ({item, index}, parallaxProps) {
        console.log(item);    
        return (
        <View style={{flex:1,}}>

                <View style={styles.card}>
                
                        <View style={{flexDirection:'row',flex:1,margin:20,justifyContent:'center',alignItems:'center' ,backgroundColor:''}}>
                        <View style={{flexDirection:'column',flex:1,justifyContent:'center',alignItems:'center',backgroundColor:''}}>
                        <Text style={{fontSize:widthPercentageToDP(3.8),fontWeight:'bold'}}>At  3:30 pm</Text>
                        <TouchableOpacity
         style={{height:heightPercentageToDP(4.65),
width:widthPercentageToDP(28.5),
padding:5,
borderWidth:1,
borderColor:'#f7f7f7',
borderRadius:50,
backgroundColor:'#b2ddc1',
marginTop:15,
}}
         onPress={()=>this.props.navigation.navigate('F')}
        
       >
         <Text style={{textAlign:"center",fontWeight:"strong",fontSize:widthPercentageToDP(3.5),color:'white'



         }} >in 2 hours</Text>
         </TouchableOpacity>
                        </View>
                        <View
                        style={{width:1,height:80,backgroundColor:'#dddddd90',margin:10,}}
                        />
                        <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:''}}>
                          
                  
                        <Image source={require('../assets/ritikaamru.jpeg') }  style={{borderRadius: heightPercentageToDP(9) / 2,
height: heightPercentageToDP(9),
width: heightPercentageToDP(9),borderWidth:2.5, borderColor:'#A1EF8B'}} />
                        
                        <Text style={{fontSize:widthPercentageToDP(3.2), textAlign:'center' ,marginTop:7,}}>RITIKA AMRU</Text>
                       
                        </View>
                        </View>
                   
                       

                        </View>
                        </View>

            
        );
    }

    render () {
        return (
          <View style={styles.container}>
            
            <View style={{ height:heightPercentageToDP(41),backgroundColor:'#b2ddc1'}}>
            <View style={{flex:0.5,justifyContent:'center',alignItems:'center'}}><Text style={{marginTop:12,fontSize:15.5,color:'#fff'}}>Appointment for today</Text></View>
            <Carousel
                sliderWidth={screenWidth}
                sliderHeight={screenWidth}
                itemWidth={screenWidth - 60}
                data={this.state.entries}
                renderItem={this._renderItem}
                hasParallaxImages={true}
                
            />
            </View>
            <View style={{flex:2,backgroundColor:'white',}}>

  <View style={styles.bottom} >
    <View style={styles.bottomitem}>
      <View style={styles.bottominneritem}>
    <Image source={require('../assets/Allapppoinment.jpeg') }  style={{width:"50%",height:'50%'}} />
    <Text style={{textAlign:'center',fontSize:widthPercentageToDP(3.6),color:'#737380'}}> All Appointment</Text>
      </View>
    
  </View>
  <View style={styles.bottomitem}>
    <View style={styles.bottominneritem}>
    <Image source={require('../assets/myprofile.jpeg') }  style={{width:"50%",height:'50%'}} />
    <Text style={{textAlign:'center',fontSize:widthPercentageToDP(3.6),color:'#737380'}}> My Profile</Text>
      </View>
      
  </View>
  <View style={styles.bottomitem}>
    <View style={styles.bottominneritem}>
    <Image source={require('../assets/Addschedule.jpeg') }  style={{width:"50%",height:'50%'}} />
    <Text style={{textAlign:'center',fontSize:widthPercentageToDP(3.6),color:'#737380'}}>Add Schedule</Text>
    
      </View>
      
  </View>
   <View style={styles.bottomitem}>
    <View style={styles.bottominneritem}>
    <Image source={require('../assets/viewhistory.jpeg') }  style={{width:"55%",height:'50%'}} />
    <Text style={{textAlign:'center',fontSize:widthPercentageToDP(3.6),color:'#737380'}}>View Patient</Text>
    
      </View>
      
      
  </View>
  <View style={styles.bottomitem}>
    <View style={styles.bottominneritem}>
    <Image source={require('../assets/viewhistory.jpeg') }  style={{width:"55%",height:'50%'}} />
    <Text style={{textAlign:'center',fontSize:widthPercentageToDP(3.6),color:'#737380'}}>View Patient</Text>
    
      </View>
      
      
  </View>

</View>



          </View>



     
          </View>
          

        );
    }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    

    backgroundColor:'#B2DDC1',
 
  },  

bottom:{

  height:"100%",

  flexDirection:'row',
  flexWrap:'wrap',
  padding:15,
  paddingTop:18,

  
},

bottomitem:{
width:widthPercentageToDP(45),
height:heightPercentageToDP(19),
padding:14,






},

bottominneritem:{

  flex:1,

  borderColor:'#c4c4c4',
  borderWidth:0.9,
  borderRadius:5,
  justifyContent:'center',
  alignItems:'center',
  backgroundColor:'white'
},
  
   
card:{
  
  height:'64%',
  alignSelf:'center',
  alignSelf:'center',
 
  alignItems:'center',
   marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
   ...StyleSheet.absoluteFillObject,

    borderWidth:1,
    borderColor:'#f7f7f7',
    borderRadius:20,
  
    backgroundColor:'white',
   
}
})


// https://github.com/oblador/react-native-parallax
// https://stackoverflow.com/questions/57593529/react-native-dynamic-image-slider-transition-opacity