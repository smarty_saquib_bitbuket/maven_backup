import React,{Component} from 'react';
import {View,Text,StyleSheet,TextInput,Button,Image,TouchableOpacity,KeyboardAvoidingView} from 'react-native';
import { Ionicons,FontAwesome,MaterialCommunityIcons,MaterialIcons} from '@expo/vector-icons'; // refer 
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { ScrollView } from 'react-native-gesture-handler';

export default class Symtoms extends React.Component{
    calling=()=>{
            alert("calling")
    
                
    }
            state={
                text:'',
                todos:[],
                status:false
            }

            AddTodos=()=>{
               
                
              var newtodos=this.state.text;
              var arr=this.state.todos
              if(this.state.text==''){
                 alert("field cannot be empty")
                 
              }
            
              
                if(this.state.text!=''){
                    if(arr.includes(newtodos)){
                        alert("already exist");  
                        this.setState({
                            text:''
                        }) 
                    }
                    else{
                    arr.push(newtodos)
                    this.setState({todos:arr})
                    console.log("else execute")
                    this.setState({
                        text:''
                    })
                }
                }
               
                this.textInput.clear()
              

               
            }
            

            DeleteTodos=(t)=>{
                   var deletearr=this.state.todos
                   var index = deletearr.indexOf(t);
                   console.log(index);
                    deletearr.splice(index,1)
                   this.setState({
                       todos:deletearr,
                      
                       

                   })
                   this.state.status=true
                   

                }

            RenderTodos=()=>{
                console.log(this.state.todos)
                console.log(this.state.text)
              
                return this.state.todos.map(t=>{

                    return(
                    
                        <View style={{borderWidth:1,borderColor:'#B2DDc1',padding:8,borderRadius:20,margin:8,backgroundColor:'#B2DDC1',flexDirection:'row'}}>
                        <Text style={{fontSize:15.5,textAlign:'left',lineHeight:20,color:'#ffffff'}}>{t}{"  "}</Text><MaterialIcons name='cancel'color={'white'} size={22}  key={t} onPress={()=>this.DeleteTodos(t)}/>
                        </View>

               
                    )
                })
        
        }
        

            render(){
                
                const keyboardVerticalOffset = Platform.OS === 'ios' ? 60 : 0
                return(


 <View style={styles.container}>
 <ScrollView style={{}}>
 <View style={styles.box}> 
  <View style={styles.boxitems}>
 <View style={{flex:1,justifyContent:'space-around',margin:5,}}>

<View ><Text style={{fontSize:20,padding:10,color:'#c4c4c4',fontWeight:'bold', textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#f8f8f8'}} >Appoinment No <Text style={{fontSize:20,padding:10,color:'#b2ddc1',fontWeight:'bold', textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#f7f7f7'}}> #AB34RT</Text></Text></View>

<View ><Text style={{fontSize:20,padding:10,color:'#c4c4c4',fontWeight:'bold'}} >Date <Text style={{fontSize:22,padding:10,color:'#000000',fontWeight:'bold', textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#c4c4c4'  }}>  Thursday ,24th Nov</Text></Text></View>

<View ><Text style={{fontSize:20,padding:10,color:'#c4c4c4',fontWeight:'bold'}} >Time<Text style={{fontSize:20,padding:10,color:'#000000',fontWeight:'bold', textShadowOffset: {width: 2,height: 2},
      textShadowRadius: 10,
      textShadowColor:'#c4c4c4'}} >   3:30 pm</Text></Text></View>

</View>



                         </View>
                         
                          </View>
                   

 
                           
   
<View style={styles.subcontainer}>


<View style={styles.image}>
<View style={{flex:1,justifyContent:'center',alignItems:'center' }}>
<Image style={{borderRadius: heightPercentageToDP(12) / 2,
height: heightPercentageToDP(12),
width: heightPercentageToDP(12), borderWidth:3,borderColor:'#9eddcf'}}
source={require('../assets/ritikaamru.jpeg')}
/>
</View>
</View>
<View style={styles.text_with_button}>
<View style={{flex:1,justifyContent:"center"}}>

<Text style={{color:'#000000',fontSize:20,padding:2}}> Erika Mars</Text>
<Text style={{color:'#9eddcf',fontSize:20,padding:2}}> Lactation Expert</Text>

</View>

</View>

</View>
   
                  

<View>
<View style={{margin:15}}><Text style={{fontSize:21.5,margin:8}}>Symptoms</Text></View></View>
            
    
 <View style={styles.input}>
            

                  <TextInput
                        style={{borderBottomWidth:1,height:"55%",borderBottomColor:'silver',width:widthPercentageToDP(78)}}
                        placeholder='write somethimg!!!'
                        onChangeText={(text)=>this.setState({text})}
                        ref={input => { this.textInput = input }}

                        />
                                    

                        <View style={{justifyContent:'center',alignItems:'center'}}>
                         <Ionicons name='md-add-circle-outline' size={48} color={'#b2ddc1'} onPress={this.AddTodos}/>
                         </View>
                        </View>
                        

                  
                  
                        <View style={{flexDirection:'row',flexWrap:'wrap',justifyContent:'space-around'}}>
                        {this.RenderTodos()}
                        </View>



                        <KeyboardSpacer/>  
                        </ScrollView>
                    </View>
                );
            }

}


const styles=StyleSheet.create({
    container:{
      
       
        flex:1,
        backgroundColor:'white',
        paddingTop:heightPercentageToDP(4)
    },

box:{
flex:0.42,
backgroundColor:'white',
justifyContent:"space-evenly",
alignItems:"center",

flexDirection:'row',

},

boxitems:{
   
    height:heightPercentageToDP(23),
 width:widthPercentageToDP(89),
    margin:22,
    
    borderWidth:0.9,
    borderRadius:8,
    borderColor:'#c4c4c4'


    
    
    },
    

    subcontainer:{
        flex:0.25,
        backgroundColor:'white',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
 
    paddingBottom:13
      
      
        
      
        
    },
   
    text_with_button:{
        flex:3.8,
        
    },
    
 input:{
flexDirection:'row',
flex:1,
justifyContent:'space-around',




padding:10
    },
    image:{
        flex:2,
        
        margin:8,
        flexDirection:'row',
       height:"90%"
    
    },
    
})