
import React,{ Component } from 'react'
import {View,Text,Image, StyleSheet,ScrollView,StatusBar,FlatList} from 'react-native'
import { Provider as PaperProvider } from 'react-native-paper';
import { Button } from 'react-native-paper';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Dimensions } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';


 export default class HomeScreen extends Component {
    static navigationOptions = { header: null };
   
    componentDidMount() {          // remove default status bar 
        StatusBar.setHidden(true);
     }


    static navigationOptions = {
        header: null
    }
  constructor(props) {          //
    super(props);
    this.state = {

      show_App: false
      
    };
  }

  onDone = () => {
    this.setState({ show_App: true });
  };

  onSkip = () => {
    this.setState({ show_App: true });
  };
  render() {
    
                        return (
                            <View style={{ backgroundColor:'red' ,height:280,width:'100%'}}>
                              
                            <View style={{flex:2,borderRadius:40,borderWidth:5,borderColor:'yellow',margin:33 }}>
                           
                             
                            <FlatList  
                            
            data={[  {title:'DATE : TODAY',subtitle:' AT :3.30PM ',avtar:require('../assets/ritikaamru.jpeg'),
            },


            ]}  
            renderItem={({item}) =>  

         
                        <View style={styles.card}>

                          
                        <View style={{flexDirection:'row',margin:24,}}>
                       
                        <View style={{flex:2,}}>
                        <Text style={{fontSize:16, padding:5,fontWeight:'bold'}}>{item.title}</Text>
                        <Text style={{color:'#C4C4C4', fontSize:17,padding:5,fontWeight:''}}>{item.subtitle}</Text>
                        
                        <Text style={{color:'silver',fontSize:14}}>{item.experience}</Text>
                        <View>
                        <View>
                          <View style={{flex:1,flexDirection:'column'}}>
                    
                        <Text style={{color:'silver',fontSize:14}}>{item.experience}</Text>
                        </View>
                        </View>
                        </View>
                        </View>
                        
                       
                        <View style={{flexDirection:'row',flex:1,justifyContent:'space-around',}}>
                            <View
                                style={{
                                borderLeftWidth: 1,
                                borderLeftColor: '#dddddd80',
                                }}
                                />
<View>
                        <Image
                        style={{width:65,height:65,borderRadius:100,borderWidth:3, borderColor:'#A1EF8B'}}
                        source={item.avtar}
                        />
                        
                        <Text style={{fontSize:11, textAlign:'center' ,marginTop:8,}}>RITIKA AMRU</Text>
                        </View>
                        
             
                        </View>
                        </View>
                        
                      
                       
                       </View>
                      
                      }  
                    ItemSeparatorComponent={this.renderSeparator}  
           
        
            />  
            
               
             </View>

                               
                               
                            <View style={{flex:1 ,marginBottom:-20}}><AppIntroSlider
                                    slides={slides}
                                    /></View>
                            
                            </View>  

                        
        
      );
    }
  }

const styles = StyleSheet.create({

  mainapp: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius:50
  },
  title: {
    fontSize: 26,
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
    borderRadius:500
  },
  text: {
    color: '#fff',
    fontSize: 20,
  },
  image: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
    borderWidth:5,
    

    width:'80%',
    height:200,
    borderRadius:500
  
    
    
  },

  card:{
    
    flex:2,
    borderWidth:1,
    borderColor:'#f7f7f7',
    margin: 10,
    borderRadius:5,
  
    
    shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 4,
},
shadowOpacity: 0.32,
shadowRadius: 5.46,
backgroundColor:'red',
height:140,
elevation: 9,
borderRadius:20

},
    container:{

        flex:1,
  
    width:'100%',
     
     shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    
    },
    shadowRadius: 5,
    shadowOpacity: 1.0

    },
});

const slides = [
  {
    key: 's1',
    title: '',
    text: '',
    
    titleStyle: styles.title,
    textStyle: styles.text,
    imageStyle: styles.image,

   

  },
  {
    key: 's2',
    title: ' ',
    text: '',
  
    titleStyle: styles.title,
    textStyle: styles.text,
    imageStyle: styles.image,
   
    borderRadius:500
  },
  {
    key: 's3',
    title: 'J ',
    text: '',
    borderWidth:10,
    borderColor:'black',
    
    titleStyle: styles.title,
    textStyle: styles.text,
    imageStyle: styles.image,
   
  },
  {
    key: 's4',
    title: '',
    text: ' ',
    
    titleStyle: styles.title,
    textStyle: styles.text,
    imageStyle: styles.image,
  
  }
];