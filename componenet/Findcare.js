import React,{Component} from 'react'
import * as Font from 'expo-font';
import {View ,Text,StyleSheet,FlatList,Alert,Image} from 'react-native'
import { Searchbar, IconButton } from 'react-native-paper';
import { Ionicons,FontAwesome,MaterialCommunityIcons,MaterialIcons} from '@expo/vector-icons'; 
import Findcarechild from './Findcarechild'
import { createBottomTabNavigator } from 'react-navigation-tabs';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen';
import { TouchableOpacity } from 'react-native-gesture-handler';
 class Findcare extends React.Component{


  componentDidMount() {
    Font.loadAsync({
      'open-sans-Regular': require('../assets/fonts/SourceSerifPro-Regular.ttf'),
    });
    
  }
    
    state={
                data:[
                    {
                        avtar:require('../assets/generalhealth.png'),
                        text:'General Health'
                      
 
                   },
                     {
                      avtar:require('../assets/nutrition.png'),
                      text:'Nutrition '

 
                     },
                     {
                      avtar:require('../assets/emotionalhealth.png'),
                      text:'Emotional Health'

 
                     },
                     {
                      avtar:require('../assets/pregnancyandpostpartum.png'),
                      text:'Pregnancy & Postpartum'

 
                     },
                     {
                      avtar:require('../assets/sexandcontraceptive.png'),
                      text:'Sex and Contraceptive'

 
                     },
                     {
                      avtar:require('../assets/fertility.png'),
                      text:'Fertility'

 
                     },
                    

 
                    
                
                ]
    }
    renderSeparator = () => {  
        return (  
            <View  
                style={{  
                    height: 1,  
                   marginLeft:30,
                   marginRight:30,
                   backgroundColor:'#c4c4c4'
                  
                  
                }}  
            />  
        );  
    };  
    //handling onPress action  
    getListViewItem = (item) => {  
        Alert.alert(item.key);  
    }  
    redirect=()=>{
      alert('fghj')
    }
    render(){
 
        return(
            <View style={styles.container}>
                <View style={{flexDirection:'column',justifyContent:'space-evenly',flex:1.5,backgroundColor:'white'}}>
               <View  ><Text style={{fontSize:heightPercentageToDP(3.1),alignSelf:'center',color:'#757171',fontFamily:'open-sans-Regular' }}>FIND CARE</Text></View>
         
                  <Searchbar 
                  onPress={()=>Alert.alert('fg')}
                     style={{flexDirection:'row'}} placeholder="Search by condition or speciality"
                         />
                   
                          </View>



               <View style={{flex:6.5,borderWidth:heightPercentageToDP(0.1),borderColor:'#c4c4c4',borderRadius:5,width:'100%'}}>
                  <FlatList  
                    data={this.state.data} 
                      renderItem={({item})=>
                      <TouchableOpacity onPress={()=>this.props.navigation.navigate('Findcarechild')}>
                        <View style={{flexDirection:'row',padding:heightPercentageToDP(3),justifyContent:'flex-start',alignItems:'center',}}>
                           <Image
                     
                              style={{width:widthPercentageToDP(12.5),height:heightPercentageToDP(6.6),
                                }}
                                  source={item.avtar}
                                 />
                                 <Text style={{lineHeight:46,fontSize:heightPercentageToDP(2),marginLeft:'10%'}}>{item.text}</Text>
                                  </View>
                                  </TouchableOpacity>
                                        }
                                          ItemSeparatorComponent={this.renderSeparator}  
                                            />  
                                                </View>
               
                                                   </View>

        );
    }

}



// class Community extends React.Component {
//     render() {
//       return (
//         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',backgroundColor:'white'}}>
//           <Text>Under working</Text>
//         </View>
//       );
//     }
//   }
 
//   class general extends React.Component {
//     render() {
//       return (
//         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//           <Text>general page</Text>
//         </View>
//       );
//     }
//   }
  



// const TabNavigator = createBottomTabNavigator({
  
  
//   Findcare: {
//         screen:Tab_with_list,
//         navigationOptions:{
//             tabBarLabel:'Find Care',
//             tabBarIcon:({tintColor})=>(
//                 <FontAwesome name='heartbeat' color={tintColor} size={25}/>
              
//             )
            
//         }

//     },



//     Community: {
//         screen:Community,
//         navigationOptions:{
//             tabBarLabel:'Community',
          
//             tabBarIcon:({tintColor})=>(
//                 <MaterialCommunityIcons name='file-document' color={tintColor} size={25}/>
//             )
            
//         }
//     },



//     general: {
//         screen:general,
//         navigationOptions:{
//             tabBarLabel:'general',
//             tabBarIcon:({tintColor})=>(
//             <MaterialIcons name='person' color={tintColor} size={25}/>
//             )
            
//         }
//     },
   
//   },





//   {
//       initialRouteName:'Community',
    
//       tabBarOptions:{
//           activeTintColor:'#9eddcf',
//           inactiveTintColor:'gray',
//           style:{
            
//             borderTopWidth:0,
//             paddingBottom:5,
//             backgroundColor:'white'
//           },
          
          
//       }

//     } );



    
// export default createAppContainer(TabNavigator);
  

  
const AppNavigator = createStackNavigator({
  Findcare:{
        screen:Findcare,
        navigationOptions: {
        header: null // Will hide header for all screens of current stack 
          
        }
              },
                           
    Findcarechild:{
    screen:Findcarechild,
    navigationOptions: {
     header: null, // Will hide header for all screens of current stack 
    
     }

        },  
        
        });

              // This code let you hide the bottom app bar when "CustomHide" is rendering
AppNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible;
  if (navigation.state.routes.length > 1) {
    navigation.state.routes.map(route => {
      if (route.routeName === "Findcarechild") {
        tabBarVisible = false;
      } else {
        tabBarVisible = true;
      }
    });
  }

  return {
    tabBarVisible
  };
};
     
export default AppNavigator

const styles=StyleSheet.create({
container:{
    flex:1,
    margin:0,
    padding:20,
   
   backgroundColor:'white'
   
},


})



