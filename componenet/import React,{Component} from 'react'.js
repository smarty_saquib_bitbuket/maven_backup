import React,{Component} from 'react'
import * as Font from 'expo-font';
import {View ,Text,StyleSheet,FlatList,Alert,Image,Modal, TouchableHighlight,button} from 'react-native'
import { Searchbar, IconButton } from 'react-native-paper';
import { Ionicons,FontAwesome,MaterialCommunityIcons,MaterialIcons,AntDesign,Entypo} from '@expo/vector-icons'; // refer 
import Triangle from 'react-native-triangle';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import styled from 'styled-components';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

class Tab_with_list extends React.Component{

  componentDidMount() {
    Font.loadAsync({
      'open-sans-Regular': require('../assets/fonts/SourceSerifPro-Regular.ttf'),
    });
  }
    
    renderSeparator = () => {  
        return (  
            <View  
                style={{  
                    height: 1,  
                   marginLeft:30,
                   marginRight:30,
                   backgroundColor:'#c4c4c4'
                  
                  
                }}  
            />  
        );  
    };  
    //handling onPress action  
    getListViewItem = (item) => {  
        Alert.alert(item.key);  
    }  




    
        state = {
          modalVisible: false,
        };
      
        setModalVisible(visible) {
          this.setState({modalVisible: visible});
        }
      

    render(){
 

        
        return(

          
     
            <View style={styles.container }>
                <View style={{}}>
        
                    <View style={{height:'25%',justifyContent:'center',alignItems:'center'}}>
          
             
             
             
                          <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('please Select the date');
          }}>


            
            <View style={styles.container1}>
              
            <View style={styles.TriangleShapeCSS} />
               <View style={styles.card}>
                   <View style={{flex:1,flexDirection:'row',margin:15,backgroundColor:'white'}}>
                       <View style={{flex:1,flexDirection:'column',justifyContent:"space-evenly",alignItems:'center'}}>
                       <View style={{paddingBottom:10}}><Text style={{textAlign:'center'}}>FRI</Text></View>

                         <TouchableHighlight
                           onPress={() => {
                           this.setModalVisible(!this.state.modalVisible);
                               }}>
                  
                            <Text style={{textAlign:'center',fontSize:19.5,fontWeight:'bold'}}>04</Text>
                              </TouchableHighlight>
                                   </View>










            <View style={{flex:1,flexDirection:'column',justifyContent:"space-evenly"}}>
            <View style={{paddingBottom:10}}><Text style={{textAlign:'center',}}>SAT</Text></View>
                 <TouchableHighlight
                   onPress={() => {
                     this.setModalVisible(!this.state.modalVisible);
                        }}>
                       <Text style={{textAlign:'center',fontSize:18,fontWeight:'bold'}}>05</Text>
                          </TouchableHighlight>
                           </View>

                            <View style={{flex:1,flexDirection:'column',justifyContent:"space-evenly"}}>
                            <View style={{paddingBottom:10}}><Text style={{textAlign:'center',}}>SUN</Text></View>

                                 <TouchableHighlight
                                 onPress={() => {
                                  this.setModalVisible(!this.state.modalVisible);
                                       }}>
                                   <Text style={{textAlign:'center',fontSize:18,fontWeight:'bold'}}>06</Text>
                                  </TouchableHighlight>
                                          </View>

                                          <View style={{flex:1,flexDirection:'column',justifyContent:"space-evenly"}}>
                                          <View style={{paddingBottom:10}}><Text style={{textAlign:'center',}}>MON</Text></View>

                                             <TouchableHighlight
                                              onPress={() => {
                                                this.setModalVisible(!this.state.modalVisible);
                                                 }}>
                                            <Text style={{textAlign:'center',fontSize:18,fontWeight:'bold'}}>07</Text>
                                               </TouchableHighlight>
                                                     </View>

                                             <View style={{flex:1,flexDirection:'column',justifyContent:"space-evenly"}}>
                                             <View style={{paddingBottom:10}}><Text style={{textAlign:'center'}}>TUE</Text></View>

              <TouchableHighlight
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}>
                <Text style={{textAlign:'center',fontSize:18,fontWeight:'bold'}}>08</Text>
              </TouchableHighlight>
            </View>
            <View style={{flex:1,flexDirection:'column',justifyContent:"space-evenly"}}>
            <View style={{paddingBottom:10}}><Text style={{textAlign:'center'}}>WED</Text></View>

              <TouchableHighlight
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}>
                <Text style={{textAlign:'center',fontSize:18,fontWeight:'bold'}}>09</Text>
              </TouchableHighlight>
            </View>
            <View style={{flex:1,flexDirection:'column',justifyContent:"space-evenly"}}>
            <View style={{paddingBottom:10}}><Text style={{textAlign:'center'}}>THU</Text></View>

              <TouchableHighlight
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}>
                <Text style={{textAlign:'center',fontSize:18,fontWeight:'bold'}}>10</Text>
              </TouchableHighlight>
            </View>
            </View>
          </View>
          </View>
        </Modal>

     <View style={{height:'50%', justifyContent:"center",alignSelf:"center"}}>   
<View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <TouchableHighlight   style={styles.button}
          onPress={() => {
            this.setModalVisible(true);
          }}>
          <Text style={{textAlign:'center',fontSize:16.8,color:'#fff',}}>Friday on 4th October, 2009</Text>
        </TouchableHighlight>




  
</View>      
</View>
      </View>
               

 

 
         <View style={{margin:22}}>
                          
         <FlatList 
        
         data={[
         
            
            {key: '7.00 pm - 7.30 pm'},
            {key: '8.00 pm - 8.30 pm'},
            {key: '9.00 pm - 9.30 pm'},
            {key: '10.00 pm - 10.30 pm'},
            {key: '11.00 pm - 11.30 pm'},
            {key: '12.00 am - 12.30 am'},

          ]}
          renderItem={({item}) => 
          <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
          <Text style={styles.item}>{item.key}
          </Text>
      <View style={{}}><AntDesign name='right' size={27} color={'#b2ddc1'}/></View>
          </View>
        }
        />
      </View>
  
               </View>
                                                   </View>

        );
    }

}



class Community extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',backgroundColor:'white'}}>
          <Text>Under working</Text>
        </View>
      );
    }
  }
 
  class general extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text>general page</Text>
        </View>
      );
    }
  }
  



const TabNavigator = createBottomTabNavigator({
  
  
    Tab_with_list: {
        
        screen:Tab_with_list,
        navigationOptions:{
            tabBarLabel:'Find Care',
            tabBarIcon:({tintColor})=>(
                <FontAwesome name='heartbeat' color={tintColor} size={25}/>
              
            )
            
        }

    },



    Community: {
        screen:Community,
        navigationOptions:{
            tabBarLabel:'Community',
          
            tabBarIcon:({tintColor})=>(
                <MaterialCommunityIcons name='file-document' color={tintColor} size={25}/>
            )
            
        }
    },



    general: {
        screen:general,
        navigationOptions:{
            tabBarLabel:'general',
            tabBarIcon:({tintColor})=>(
            <MaterialIcons name='person' color={tintColor} size={25}/>
            )
            
        }
    },
   
  },





  {
      initialRouteName:'Community',
    
      tabBarOptions:{
          activeTintColor:'#9eddcf',
          inactiveTintColor:'gray',
          style:{
            
            borderTopWidth:0,
            paddingBottom:5,
            backgroundColor:'white'
          },
          
          
      }

    } );



    
export default createAppContainer(TabNavigator);
  
  

const styles=StyleSheet.create({

  container1: {
    flex: 1,
    
    alignItems: 'center',
  
    paddingTop:80,
  
    backgroundColor:'#f9f9f9',
    opacity:0.65,
  },
 


  card:{

    backgroundColor:'white',
    height:"14%",
    width:'92.5%',
    borderRadius:5,
    borderColor:'white',
    borderWidth:0.9,
    elevation:1

  },
TriangleShapeCSS: {
 
  width: 0,
  height: 0,
  borderLeftWidth: 12.5,
  borderRightWidth: 12.5,
  borderBottomWidth: 20,
  borderStyle: 'solid',
  backgroundColor: 'transparent',
  borderLeftColor: 'transparent',
  borderRightColor: 'transparent',
  borderBottomColor: '#c4c4c4',
  borderTopColor:'black',
  overflow:"hidden",
  position: 'relative',
},
 
container: {
  flex: 1,
  height:'100%',
  backgroundColor:'#fff'
 },
 item: {
   margin:12,
   fontSize: 18,
   height: 35,
   color:'#727d85',
   
 },

 triangle: {
  width: 0,
  height: 0,
  backgroundColor: 'green',
  borderStyle: 'solid',
  borderLeftWidth: 50,
  borderRightWidth: 50,
  borderBottomWidth: 100,
  borderLeftColor: 'black',
  borderRightColor: 'black',
  borderBottomColor: 'red',
  marginTop:80
},



 button:{
    height:50,
    width:320,
    borderWidth:1,
    borderColor:'#9eddcf',
    borderRadius:50,
    
    backgroundColor:'#9edccf',
    justifyContent:"center",
    shadowColor: "#c4c4c4",
    shadowOffset: {
        width: 0,
        height: 5,
    },
    shadowOpacity: 0.50,
    shadowRadius: 12,
    
    elevation: 2,
    
    }
})



