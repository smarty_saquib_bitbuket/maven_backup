import React from 'react';
import {AppRegistry} from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import SplashScreen from './components/SplashScreen';
import HomeScreen from './components/HomeScreen';
import * as Font from 'expo-font';

class Main extends React.Component{
  constructor(props){
    super(props);
    Font.loadAsync({
      'open-sans-Regular': require('./fonts/SourceSerifPro-Regular.ttf'),
    });
    this.state={
      currentScreen:'SplashScreen'
      
    };
    console.log('start doing some task for 3 second');
    setTimeout(()=>{
      console.log('Done some task for about 3 second');
      this.setState({
        currentScreen:'HomeScreen'
      })
    },2000)
  }
  render()
      {
        const{currentScreen}=this.state
        let mainscreen=currentScreen==='SplashScreen'?<SplashScreen/>:<HomeScreen/>
        return mainscreen

    }

}
export default Main
AppRegistry.registerComponent('saquib',()=>Main)